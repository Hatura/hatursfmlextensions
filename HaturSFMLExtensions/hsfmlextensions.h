#pragma once

#include "HConsole/hconsole.h"
#include "HAnimatedSprite/hanimatedsprite.h"
#include "HGUI/hgui.h"

namespace HSF {

class HSFMLExtensions
{
public:
    HSFMLExtensions() = delete;
};

}
