#pragma once

#include <vector>
#include <unordered_map>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Rect.hpp>

namespace HSF {
namespace GFX {

/**
 * @brief The HAnimatedSprite class
 *
 * A simple animated Sprite, file format for the key frames must be:
 * 1    2   3   4   5
 * 6    7   8   9   10
 * 11   12  13  14  15
 * with variable row/column size.
 *
 */
class HAnimatedSprite final : public sf::Drawable
{
public:
    HAnimatedSprite(int rows, int columns, const sf::Texture& texture);

    // Inherited from sf::Drawable
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    // Methods
    void RegisterAnimation(const std::string& animationName, int keyFrames[], int numKeyFrames);
    bool SetAnimation(const std::string& animationName, int startKeyFrame = 0);

    void Update(float amount);

    // [...] for controlling sf::Sprite
    void SetPosition(float x, float y);
    void SetPosition(const sf::Vector2f& position);

    void SetRotation(float angle);

    void SetScale(float scaleX, float scaleY);
    void SetScale(const sf::Vector2f& scale);

    // [...] as getters
    sf::Vector2f GetPosition() const;
    float GetRotation() const;

    const std::vector<std::string> GetRegisteredAnimations() const;
    size_t GetNumRegisteredAnimations() const;

private:
    // Called on Constructor, sets animation to all possible key frames (rows x columns)
    void SetupDefaultAnimation();

    const sf::Texture&  m_texture;
    sf::Sprite          m_sprite;

    int                 m_rows;             // horizontal
    int                 m_columns;          // vertical

    int                 m_currentFrame;     // current keyframe
    float               m_frameProgress;    // used to calculate the next keyframe in update

    std::vector<sf::IntRect>                                    m_allKeyFrames;
    std::vector<sf::IntRect>                                    m_currentAnimation;
    std::unordered_map<std::string, std::vector<sf::IntRect>>   m_animations;
};

} // namespace GFX
} // namespace HSF
