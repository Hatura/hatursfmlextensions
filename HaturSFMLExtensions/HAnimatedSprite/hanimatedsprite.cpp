#include "hanimatedsprite.h"

namespace HSF {
namespace GFX {

HAnimatedSprite::HAnimatedSprite(int rows, int columns, const sf::Texture& texture)
    : m_rows(rows)
    , m_columns(columns)
    , m_currentFrame(0)
    , m_texture(texture)
    , m_frameProgress(0.f) {
    SetupDefaultAnimation();
}

void HAnimatedSprite::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    target.draw(m_sprite);
}

void HAnimatedSprite::RegisterAnimation(const std::string& animationName, int keyFrames[], int numKeyFrames) {
    std::vector<sf::IntRect> animationKeyFrames;

    for (int i = 0; i < numKeyFrames; ++i) {
        animationKeyFrames.push_back(m_allKeyFrames.at(keyFrames[i]));
    }

    m_animations.insert(std::make_pair(animationName, std::move(animationKeyFrames)));
}

bool HAnimatedSprite::SetAnimation(const std::string& animationName, int startKeyFrame) {
    auto it = m_animations.find(animationName);

    if (it == m_animations.end()) {
        return false;
    }

    m_currentAnimation = it->second;
    if (startKeyFrame >= m_currentAnimation.size()) {
        m_currentFrame = static_cast<int>(m_currentAnimation.size()) - 1;
    }
    else {
        m_currentFrame = startKeyFrame;
    }

    m_sprite.setTextureRect(m_currentAnimation.at(m_currentFrame));

    return true;
}

void HAnimatedSprite::SetupDefaultAnimation() {
    int fieldWidth = m_texture.getSize().x / m_rows;
    int fieldHeight = m_texture.getSize().y / m_columns;

    m_allKeyFrames.clear();

    for (int i = 0; i < m_columns; ++i) {
        for (int n = 0; n < m_rows; ++n) {
            sf::IntRect rect;
            rect.left = n * fieldWidth;
            rect.width = fieldWidth;
            rect.top = i * fieldHeight;
            rect.height = fieldHeight;

            m_allKeyFrames.push_back(std::move(rect));
        }
    }

    m_currentFrame = 0;
    //m_maxFrames = m_rows * m_columns;

    m_sprite.setTexture(m_texture);
    m_sprite.setTextureRect(m_allKeyFrames.at(m_currentFrame));

    m_currentAnimation = m_allKeyFrames;
}

//void HAnimatedSprite::SetStartKeyFrame(int start) {
//    if (start > m_maxFrames) {
//        start = m_maxFrames;
//    }

//    m_currentFrame = start;
//}

void HAnimatedSprite::Update(float amount) {
    if (amount > 1.0f) {
        amount = 1.0f;
    }

    m_frameProgress += amount;

    if (m_frameProgress > 1.0f) {
        if (m_currentFrame + 1 >= m_currentAnimation.size()) {
            m_currentFrame = 0;
        } else {
            ++m_currentFrame;
        }
        m_frameProgress -= 1.0f;

        m_sprite.setTextureRect(m_currentAnimation.at(m_currentFrame));
    }
}

void HAnimatedSprite::SetPosition(float x, float y) {
    m_sprite.setPosition(sf::Vector2f(x, y));
}

void HAnimatedSprite::SetPosition(const sf::Vector2f& position) {
    m_sprite.setPosition(position);
}

void HAnimatedSprite::SetRotation(float angle) {
    m_sprite.setRotation(angle);
}

void HAnimatedSprite::SetScale(float scaleX, float scaleY) {
    m_sprite.setScale(scaleX, scaleY);
}

void HAnimatedSprite::SetScale(const sf::Vector2f& scale) {
    m_sprite.setScale(scale.x, scale.y);
}

sf::Vector2f HAnimatedSprite::GetPosition() const {
    return m_sprite.getPosition();
}

float HAnimatedSprite::GetRotation() const {
    return m_sprite.getRotation();
}

const std::vector<std::string> HAnimatedSprite::GetRegisteredAnimations() const {
    std::vector<std::string> animationNames;

    for (auto& animation : m_animations) {
        animationNames.push_back(animation.first);
    }

    return animationNames;
}

size_t HAnimatedSprite::GetNumRegisteredAnimations() const {
    return m_animations.size();
}

} // namespace GFX
} // namespace HSF
