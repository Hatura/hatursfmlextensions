#pragma once

namespace HSF {
namespace GUI {

enum class EButtonState {
    UP,
    DOWN,
    HOVER
};

class HButtonInterface
{
public:
    /**
     * @brief RegisterCallback
     * For left click or enter
     */
    virtual void RegisterCallback(std::function<void(HComponent*)> functionObject) = 0;

    /**
     * @brief RegisterAltCallback
     * For right click
     */
    virtual void RegisterAltCallback(std::function<void(HComponent*)> functionObject) = 0;
};

}
}
