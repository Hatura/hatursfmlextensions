#include "hcontainer.h"

#include <iostream>
#include <algorithm>
#include "hverticalcontainer.h"
#include "hhorizontalcontainer.h"
#include "habsolutecontainer.h"
#include "hwindow.h"

namespace HSF {
namespace GUI {

HContainer::HContainer()
    : m_sizeInvalidated(true)
    , m_layoutInvalidated(true)
    , m_rootContainer(false) {

}

void HContainer::PostLayout() {
    if (m_layoutInvalidated) {
        for (auto& container : m_childContainer) {
            container->PostLayout();
        }

        for (auto& component : m_childComponents) {
            component->CalculateRelativePosition();
        }

        ValidateLayout();
    }
}

void HContainer::RequestSafeRemoval(HComponent* component) {
    m_deletionFlaggedComponents.insert(component);
}

void HContainer::RequestSafeRemoval(unsigned int index) {
    if (index < m_childComponents.size()) {
        m_deletionFlaggedComponents.insert(m_childComponents[index].get());
    }
}

void HContainer::RequestSafeRemovalAll() {
    for (auto& component : m_childComponents) {
        m_deletionFlaggedComponents.insert(component.get());
    }
}

void HContainer::DrawHierachy(sf::RenderTarget& rw, bool debugRender) {
    // Remove for deletion flagged components
    RemoveFlaggedComponents();

    // Draw hierachys for child containers
    for (auto& childContainer : m_childContainer) {
        childContainer->DrawHierachy(rw, debugRender);
    }

    // Draw child components
    for (auto& childComponent : m_childComponents) {
        childComponent->Draw(rw);
        if (debugRender) {
            childComponent->DrawDebug(rw);
        }
    }

    // Draw
    Draw(rw);
    if (debugRender) {
        DrawDebug(rw);
    }
}

void HContainer::SetRoot() {
    m_rootContainer = true;
}

void HContainer::SetHGUI(HGUI* hgui) {
    m_hgui = hgui;
}

void HContainer::PropagateHGUIPointer(HGUI* hgui) {
    for (auto& container : m_childContainer) {
        container->PropagateHGUIPointer(hgui);
    }

    for (auto& component: m_childComponents) {
        component->m_hgui = hgui;
    }

    m_hgui = hgui;
}

void HContainer::RemoveFlaggedComponents() {
    if (m_deletionFlaggedComponents.size() > 0) {
        size_t totalFlaggedComponentCount = m_deletionFlaggedComponents.size();

        for (auto& component : m_deletionFlaggedComponents) {
            bool containerRemovalSuccess = false;
            bool componentRemovalSuccess = false;

            // Deleter from HContainer deque (pointer)
            HContainer* container = dynamic_cast<HContainer*>(component);
            if (container != nullptr) {
                auto containerDeleterIT = m_childContainer.end();

                for (auto it = m_childContainer.begin(); it != m_childContainer.end(); ++it) {
                    if ((*it)->Compare(container)) {
                        containerDeleterIT = it;
                    }
                }

                if (containerDeleterIT != m_childContainer.end()) {
                    (*containerDeleterIT)->RemoveAllComponents();

                    m_childContainer.erase(containerDeleterIT);

                    containerRemovalSuccess = true;
                }
            } else {
                // Is not a container..
                containerRemovalSuccess = true;
            }

            // Deleter from HComponent deque (owner)
            auto componentDeleterIT = m_childComponents.end();

            for (auto it = m_childComponents.begin(); it != m_childComponents.end(); ++it) {
                if ((*it)->Compare(component)) {
                    componentDeleterIT = it;
                }
            }

            if (componentDeleterIT != m_childComponents.end()) {
                m_childComponents.erase(componentDeleterIT);

                componentRemovalSuccess = true;
            }

            if (containerRemovalSuccess && componentRemovalSuccess) {
                --totalFlaggedComponentCount;
            }
        }

        if (totalFlaggedComponentCount > 0) {
            std::cerr << "Couldn't safely delete " << totalFlaggedComponentCount << " components in RemoveFlaggedComponents() hcontainer.cpp" << std::endl;
        }
        m_deletionFlaggedComponents.clear();

        sf::Vector2f totalSize(0.f, 0.f);

        for (const auto& component : m_childComponents) {
            totalSize += component->GetCalculatedSize();
        }

        SetSize(totalSize);

        InvalidateHierachySize();
        InvalidateHierachyLayout();
    }
}

void HContainer::RemoveAllComponents() {
    for (auto& container : m_childContainer) {
        container->RemoveAllComponents();
    }

    m_childComponents.resize(0);
    m_childContainer.resize(0);

    SetSize(sf::Vector2f(0.f, 0.f));

    InvalidateHierachySize();
    InvalidateHierachyLayout();
}

void HContainer::RecalculateHierachy() {
    if (m_sizeInvalidated) {
        sf::Vector2f totalSize(0.f, 0.f);

        static int numCalcs = 0;
        ++numCalcs;
        std::cerr << "Num calcs: " << numCalcs << std::endl;

        float widestComponent = 0.f;
        float highestComponent = 0.f;

        // For HAbsoluteContainer
        bool firstFarthestRightRecognized = false;
        bool firstFarthestBottomRecognized = false;
        float farthestRight = 0.f;
        float farthestBottom = 0.f;

        for (auto& component : m_childComponents) {
            HContainer* container = dynamic_cast<HContainer*>(component.get());
            if (container != nullptr) {
                container->RecalculateHierachy();
            } else {
                component->OnCalculateSize();
            }

            float hMargin = component->m_marginLeft + component->m_marginRight;
            float vMargin = component->m_marginTop + component->m_marginBottom;

            sf::Vector2f totalMargin(hMargin, vMargin);

            float hPadding = component->m_paddingLeft + component->m_paddingRight;
            float vPadding = component->m_paddingTop + component->m_paddingBottom;

            sf::Vector2f totalPadding(hPadding, vPadding);

            totalSize += component->GetCalculatedSize() + totalPadding + totalMargin;

            if (component->GetCalculatedSize().x + hPadding + hMargin > widestComponent) {
                widestComponent = component->GetCalculatedSize().x + hMargin + hPadding;
            }

            if (component->GetCalculatedSize().y + vPadding + vMargin > highestComponent) {
                highestComponent = component->GetCalculatedSize().y + vMargin + vPadding;
            }

            // For absolute
            if (!firstFarthestRightRecognized || GetAbsolutePosition().x + component->GetOverwrittenRelativePosition().x + component->GetCalculatedSize().x > farthestRight) {
                farthestRight = GetAbsolutePosition().x + component->GetOverwrittenRelativePosition().x + component->GetCalculatedSize().x;
                firstFarthestRightRecognized = true;
            }

            if (!firstFarthestBottomRecognized || GetAbsolutePosition().y + component->GetOverwrittenRelativePosition().y + component->GetCalculatedSize().y > farthestBottom) {
                farthestBottom = GetAbsolutePosition().y + component->GetOverwrittenRelativePosition().y + component->GetCalculatedSize().y;
                firstFarthestBottomRecognized = true;
            }
        }

        // Special cases for vertical and horizontal containers.. is this the most elegant way?
        HVerticalContainer* vContainer = dynamic_cast<HVerticalContainer*>(this);
        if (vContainer != nullptr) {
            totalSize.x = widestComponent;
        }

        HHorizontalContainer* hContainer = dynamic_cast<HHorizontalContainer*>(this);
        if (hContainer != nullptr) {
            totalSize.y = highestComponent;
        }

        HAbsoluteContainer* absContainer = dynamic_cast<HAbsoluteContainer*>(this);
        if (absContainer != nullptr) {
            totalSize.x = farthestRight;
            totalSize.y = farthestBottom;
        }

        HWindow* windowContainer = dynamic_cast<HWindow*>(this);
        if (windowContainer != nullptr) {
            totalSize.x = static_cast<float>(windowContainer->GetWindowWidth());
            totalSize.y = static_cast<float>(windowContainer->GetWindowHeight());
        }

        SetSize(totalSize);
        m_debugShape.setSize(totalSize);
        m_debugShape.setOutlineColor(sf::Color::Blue);
        m_debugShape.setOutlineThickness(2.f);

        m_sizeInvalidated = false;
    }
}

bool HContainer::IsRootContainer() {
    return m_rootContainer;
}

void HContainer::InvalidateHierachySize() {
    m_sizeInvalidated = true;

    for (auto& container : m_childContainer) {
        container->InvalidateHierachySize();
    }
}

void HContainer::InvalidateHierachyLayout() {
    m_layoutInvalidated = true;

    for (auto& container : m_childContainer) {
        container->InvalidateHierachyLayout();
    }
}

void HContainer::ValidateLayout() {
    m_layoutInvalidated = false;
}

bool HContainer::IsSizeInvalidated() const {
    return m_sizeInvalidated;
}

bool HContainer::IsLayoutInvalidated() const {
    return m_layoutInvalidated;
}

std::deque<std::unique_ptr<HSF::GUI::HComponent>>& HContainer::GetChildComponents() {
    return m_childComponents;
}

std::deque<HSF::GUI::HContainer*>& HContainer::GetChildContainer() {
    return m_childContainer;
}

bool HContainer::DistributeMouseInput(const sf::Vector2f& mousePos, bool alreadySuccessfull) {
    for (auto& childComponent : m_childComponents) {
        HContainer* container = dynamic_cast<HContainer*>(childComponent.get());
        if (container != nullptr) {
//            if (container->DistributeMouseInput(mousePos)) {
//                return true;
//            }
//            continue;
            // Call for childContainer
            alreadySuccessfull = container->DistributeMouseInput(mousePos, alreadySuccessfull);
        }
        if (childComponent->HasMouseInputReceiver()) {
            if (!alreadySuccessfull) {
                alreadySuccessfull = childComponent->GetMouseInputReceiver()->PollMouseInput(mousePos, childComponent->m_visualRect);
            } else {
                // Somewhere the input was already polled, but we still need to reset buttons in case they were mouseOvered
                childComponent->GetMouseInputReceiver()->ResetStates();
            }
        }
    }

    return alreadySuccessfull;
}

} // namespace GUI
} // namespace HSF
