#include "habsolutecontainer.h"

namespace HSF {
namespace GUI {

HAbsoluteContainer::HAbsoluteContainer() {

}

void HAbsoluteContainer::Layout(sf::Vector2f containerOffset) {
    if (IsLayoutInvalidated()) {
        bool firstPosXRecognized = false;
        bool firstPosYRecognized = false;
        float farthestTop = 0.f;
        float farthestLeft = 0.f;

        for (auto it = GetChildContainer().begin(); it != GetChildContainer().end(); ++it) {
            (*it)->Layout(GetAbsolutePosition() + GetAbsolutePosition() + (*it)->GetOverwrittenRelativePosition()); // ???
        }

        for (auto& component : GetChildComponents()) {
            if (!firstPosXRecognized || GetAbsolutePosition().x + component->GetOverwrittenRelativePosition().x < farthestLeft) {
                farthestLeft = GetAbsolutePosition().x + component->GetOverwrittenRelativePosition().x;
                firstPosXRecognized = true;
            }

            if (!firstPosYRecognized || GetAbsolutePosition().y + component->GetOverwrittenRelativePosition().y < farthestTop) {
                farthestTop = GetAbsolutePosition().y + component->GetOverwrittenRelativePosition().y;
                firstPosYRecognized = true;
            }

            component->SetPosition(GetAbsolutePosition() + containerOffset + component->GetOverwrittenRelativePosition());

            component->OnLayout();
        }

        SetPosition(sf::Vector2f(GetAbsolutePosition().x, GetAbsolutePosition().y));
    }
}

void HAbsoluteContainer::Draw(sf::RenderTarget& rw) {

}

} // namespace GUI
} // namespace HSF
