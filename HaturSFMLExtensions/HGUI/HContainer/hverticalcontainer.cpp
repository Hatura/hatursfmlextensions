#include "hverticalcontainer.h"

#include "hhorizontalcontainer.h"

namespace HSF {
namespace GUI {

HVerticalContainer::HVerticalContainer() {

}

void HVerticalContainer::SetFillParentX() {
    if (m_parentContainer != nullptr) {
        SetSize(sf::Vector2f(m_parentContainer->GetCalculatedSize().x, GetCalculatedSize().y));

        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HVerticalContainer::Layout(sf::Vector2f containerOffset) {
    if (IsLayoutInvalidated()) {
        for (auto it = GetChildContainer().rbegin(); it != GetChildContainer().rend(); ++it) {
            sf::Vector2f siblingOffset(0.f, 0.f);

            if (it != GetChildContainer().rbegin()) {
                for (auto itx = GetChildContainer().rbegin(); itx != it; ++itx) {
                    siblingOffset += (*itx)->GetCalculatedSize();
                }
            }

            HVerticalContainer* isVContainer = dynamic_cast<HVerticalContainer*>(this);
            if (isVContainer != nullptr) {
                siblingOffset.x = 0.f;
            }

            HHorizontalContainer* isHContainer = dynamic_cast<HHorizontalContainer*>(this);
            if (isHContainer != nullptr) {
                siblingOffset.y = 0.f;
            }

            (*it)->Layout(GetCalculatedSize() + containerOffset - (*it)->GetCalculatedSize() - siblingOffset);
        }


        // we need the widest element for CENTER
        float widestComponent = 0.f;

        for (const auto& component : GetChildComponents()) {
            if (component->GetCalculatedSize().x > widestComponent) {
                widestComponent = component->GetCalculatedSize().x;
            }
        }

        if (GetCalculatedSize().x > widestComponent) {
            widestComponent = GetCalculatedSize().x;
        }

        float posY = 0.f;

        HComponent* prevComponent = nullptr;

        for (auto& component : GetChildComponents()) {
            float posX = 0.f;
            if (component->GetHAlign() == EHorizontalAlign::LEFT) {
                posX = 0.f;
            } else if (component->GetHAlign() == EHorizontalAlign::CENTER) {
                posX = widestComponent / 2.f - (component->GetCalculatedSize().x + component->GetMarginLeft() + component->GetMarginRight() + component->GetPaddingLeft() + component->GetPaddingRight()) / 2.f;
            } else if (component->GetHAlign() == EHorizontalAlign::RIGHT) {
                posX = widestComponent - (component->GetCalculatedSize().x + component->GetMarginLeft() + component->GetMarginRight() + component->GetPaddingLeft() + component->GetPaddingRight());
            }

            float widthDifference = 0.f;
            float heightDifference = 0.f;

            if (m_parentContainer != nullptr) {
                HContainer* pollingParent = this;
                HContainer* grandParent = m_parentContainer;

                if (grandParent != nullptr) {
                    while (true) {
                        HVerticalContainer* vContainer = dynamic_cast<HVerticalContainer*>(grandParent);
                        HHorizontalContainer* hContainer = dynamic_cast<HHorizontalContainer*>(grandParent);

                        if (vContainer != nullptr) {
                            widthDifference += grandParent->GetCalculatedSize().x - pollingParent->GetCalculatedSize().x;
                        }
                        else if (hContainer != nullptr) {
                            heightDifference += grandParent->GetCalculatedSize().y - pollingParent->GetCalculatedSize().y;
                        }

                        if (grandParent->m_parentContainer != nullptr) {
                            pollingParent = grandParent;
                            grandParent = pollingParent->m_parentContainer;
                        }
                        else {
                            break;
                        }
                    }
                }
            }

            // Margin
            // Horizontal
            float margX = 0.f;
            margX += component->m_marginLeft;

            posX += margX;

            // Vertical
            float margY = 0.f;
            if (prevComponent != nullptr) {
                margY += prevComponent->m_marginBottom;
            }
            margY += component->m_marginTop;

            posY += margY;

            // Padding
            // Horizontal
            float padX = 0.f;
            padX += component->m_paddingLeft;

            posX += padX;

            // Vertical
            float padY = 0.f;
            if (prevComponent != nullptr) {
                padY += prevComponent->m_paddingBottom;
            }
            padY += component->m_paddingTop;

            posY += padY;



            component->SetPosition(sf::Vector2f(posX + containerOffset.x - widthDifference, posY + containerOffset.y - heightDifference));

            component->OnLayout();

            posY += component->GetCalculatedSize().y;

            prevComponent = component.get();
        }
    }
}

void HVerticalContainer::Draw(sf::RenderTarget& rw) {

}

} // namespace GUI
} // namespace HSF
