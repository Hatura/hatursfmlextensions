#pragma once

#include "HGUI/hcomponent.h"

#include <memory>
#include <deque>
#include <set>
#include <iostream>

namespace HSF {
namespace GUI {

class HGUI;
class HWindow;

enum class EContainerPosition {
    Before,
    After
};

class HContainer : public HComponent
{
public:
    HContainer();
    virtual ~HContainer() = 0 { }

    friend class HGUI;
    friend class HWindow;

    // Pure virtual methods
    virtual void Layout(sf::Vector2f containerOffset) = 0;

    // Virtual methods
    virtual void PostLayout();

    // Methods
    /**
     * @brief AddComponent
     *
     * declared in header to allow type specific chaining
     */
    template<class T>
    T* AddComponent(HComponent* relativeTo = nullptr, EContainerPosition position = EContainerPosition::After) {
        static_assert(std::is_base_of<HComponent, T>::value, "AddComponent(): T must be of subclass HComponent");

        std::unique_ptr<T> component(new T());

        component->m_parentContainer = this;

        // TODO: Don't add HWindows to Containers..

        if (relativeTo != nullptr) {
            auto it = m_childComponents.end();
            for (auto itx = m_childComponents.begin(); itx != m_childComponents.end(); ++itx) {
                if ((*itx)->Compare(relativeTo)) {
                    it = itx;
                }
            }
            if (it == m_childComponents.end()) {
                // Error, not found!
            }
            else {
                if (position == EContainerPosition::Before) {
                    if (it != m_childComponents.begin()) {
                        --it;
                    }
                    m_childComponents.insert(it, std::move(component));
                }
                else if (position == EContainerPosition::After) {
                    if (it + 1 != m_childComponents.end()) {
                        ++it;
                    }
                    m_childComponents.insert(it, std::move(component));
                }
            }
        }
        else {
            m_childComponents.push_back(std::move(component));
        }

        HContainer* container = dynamic_cast<HContainer*>(m_childComponents.back().get());
        if (container != nullptr) {
            m_childContainer.push_back(container);

            // Propagate hgui pointer
            if (m_hgui != nullptr) {
                PropagateHGUIPointer(m_hgui);
            }
        }

        InvalidateHierachySize();
        InvalidateHierachyLayout();

        return dynamic_cast<T*>(m_childComponents.back().get());
    }

    template<class T>
    T* AddComponent(std::unique_ptr<T> component, HComponent* relativeTo = nullptr, EContainerPosition position = EContainerPosition::After) {
        static_assert(std::is_base_of<HComponent, T>::value, "AddComponent(): T must be of subclass HComponent");
        component->m_parentContainer = this;

        // TODO: Don't add HWindows to Containers..

        if (relativeTo != nullptr) {
            auto it = m_childComponents.end();
            for (auto itx = m_childComponents.begin(); itx != m_childComponents.end(); ++itx) {
                if ((*itx)->Compare(relativeTo)) {
                    it = itx;
                }
            }
            if (it == m_childComponents.end()) {
                // Error, not found!
            }
            else {
                if (position == EContainerPosition::Before) {
                    if (it != m_childComponents.begin()) {
                        --it;
                    }
                    m_childComponents.insert(it, std::move(component));
                }
                else if (position == EContainerPosition::After) {
                    if (it + 1 != m_childComponents.end()) {
                        ++it;
                    }
                    m_childComponents.insert(it, std::move(component));
                }
            }
        }
        else {
            m_childComponents.push_back(std::move(component));
        }

        HContainer* container = dynamic_cast<HContainer*>(m_childComponents.back().get());
        if (container != nullptr) {
            m_childContainer.push_back(container);

            // Propagate hgui pointer
            if (m_hgui != nullptr) {
                PropagateHGUIPointer(m_hgui);
            }
        }

        InvalidateHierachySize();
        InvalidateHierachyLayout();

        return dynamic_cast<T*>(m_childComponents.back().get());
    }

    /**
     * @brief RequestSafeRemoval
     * Request a removal called at the next draw update
     */
    void RequestSafeRemoval(HComponent* component);
    void RequestSafeRemoval(unsigned int index);
    void RequestSafeRemovalAll();

    /**
     * @brief InvalidateHierachySize & InvalidateHierachyLayout
     *
     * Can be but should NOT be called from extern because it only works if called from the farthest parent.
     * Use RequestSizeRecalculation() & RequestLayoutInvalidation() from HComponent instead, calls this
     */
    void InvalidateHierachySize();
    void InvalidateHierachyLayout();

    /**
     * @brief RecalculateHierachy
     * Maybe make this virtual to save some calculation time (remove dynamic_casts)
     */
    void RecalculateHierachy();

    bool IsRootContainer();

protected:
    void ValidateLayout();

    bool IsSizeInvalidated() const;
    bool IsLayoutInvalidated() const;

    std::deque<std::unique_ptr<HComponent>>& GetChildComponents();
    std::deque<HContainer*>& GetChildContainer();

private:
    std::deque<std::unique_ptr<HComponent>> m_childComponents;
    std::deque<HContainer*> m_childContainer;

    std::set<HComponent*> m_deletionFlaggedComponents;

    bool m_rootContainer;

    bool m_sizeInvalidated;
    bool m_layoutInvalidated;

    // Inherited from HComponent
    virtual bool DistributeMouseInput(const sf::Vector2f& mousePos, bool alreadySuccessfull);
    virtual void DrawHierachy(sf::RenderTarget& rw, bool debugRender = false);
    void OnCalculateSize() override { }
    void OnLayout() override { }

    // Used from HGUI
    void SetRoot();
    void SetHGUI(HGUI* hgui);

    void PropagateHGUIPointer(HGUI* hgui);

    void RemoveAllComponents();
    void RemoveFlaggedComponents();
};

} // namespace GUI
} // namespace HSF
