#include "hwindow.h"

#include "HGUI/hgui.h"
#include <iostream> // temp

namespace HSF {
namespace GUI {

HWindow::HWindow(unsigned int width, unsigned int height, const sf::Font& font)
    : m_titleBar(this, font, std::string("Titelbar yoyo"))
    , m_drawIndex(0) {
    AddMouseInputReceiver();

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftUp, [this] (HComponent* self) {
        m_hgui->BringWindowToFront(this);
    });

    m_window.create(width, height);

    m_windowBackground.setSize(sf::Vector2f(width-2, height-2));
    m_windowBackground.setPosition(1, 1);
    m_windowBackground.setOutlineColor(sf::Color::Yellow);
    m_windowBackground.setOutlineThickness(1.0f);

    m_titleBar.AddCloseButton(this, font);

    m_titleBar.GetMouseInputReceiver()->RegisterMoveFunction(EMouseMoveEvent::OnMouseLeftMove, [this] (HTitleBar* self, const sf::Vector2f& relative, const sf::Vector2f& absolute) {
        SetAbsolutePosition(sf::Vector2f(GetAbsolutePosition().x + relative.x, GetAbsolutePosition().y + relative.y));
    });
}

unsigned int HWindow::GetWindowWidth() const {
    return m_window.getSize().x;
}

unsigned int HWindow::GetWindowHeight() const {
    return m_window.getSize().y;
}

bool HWindow::DistributeMouseInput(const sf::Vector2f& mousePos, bool alreadySuccessfull) {
    // - GetAbsolutePosition() to get the actual transformed position
    if (!alreadySuccessfull) {
        // Takes care of resetting mouseover states itself
        alreadySuccessfull = m_titleBar.PollMouseInputForButtons(alreadySuccessfull);
    }

    if (!alreadySuccessfull) {
        alreadySuccessfull = m_titleBar.GetMouseInputReceiver()->PollMouseInput(mousePos, m_titleBar.GetBoundingBox());
    } else {
        m_titleBar.GetMouseInputReceiver()->ResetStates();
    }

    alreadySuccessfull = HContainer::DistributeMouseInput(mousePos - GetAbsolutePosition(), alreadySuccessfull);

    if (!alreadySuccessfull) {
        alreadySuccessfull = GetMouseInputReceiver()->PollMouseInput(mousePos, m_visualRect);
    } else {
        GetMouseInputReceiver()->ResetStates();
    }

    return alreadySuccessfull;
}

void HWindow::Layout(sf::Vector2f containerOffset) {
    if (IsLayoutInvalidated()) {
        for (auto it = GetChildContainer().begin(); it != GetChildContainer().end(); ++it) {
            (*it)->Layout((*it)->GetOverwrittenRelativePosition());
        }

        for (auto& component : GetChildComponents()) {
            component->SetPosition(component->GetOverwrittenRelativePosition());

            component->OnLayout();
        }

        SetPosition(sf::Vector2f(GetAbsolutePosition().x, GetAbsolutePosition().y));
        m_debugShape.setPosition(0, 0); // kinda hackish

        // Update titlebar width, TODO: Move this when Caluclate is rewritten..
        m_titleBar.SetWidth(GetCalculatedSize().x);

        // Update titlebar position
        m_titleBar.SetPosition(GetAbsolutePosition());
    }
}

void HWindow::DrawHierachy(sf::RenderTarget& rw, bool debugRender) {
    // Remove for deletion flagged components
    RemoveFlaggedComponents();

    m_window.clear(sf::Color(255, 255, 255, 140));

    // Draw hierachys for child containers
    for (auto& childContainer : m_childContainer) {
        childContainer->DrawHierachy(m_window, debugRender);
    }

    // Draw child components
    for (auto& childComponent : m_childComponents) {
        childComponent->Draw(m_window);
        if (debugRender) {
            childComponent->DrawDebug(m_window);
        }
    }

    if (debugRender) {
        DrawDebug(m_window);
    }

    m_window.display();

    // Draw self
    Draw(rw);
}

//void HWindow::OnLeftMouseButtonDown() {
//    m_hgui->BringWindowToFront(this);
//}

void HWindow::Draw(sf::RenderTarget& rw) {
    //m_window.draw(m_windowBackground);

//    sf::Texture tex;
//    tex.loadFromFile("Data/Textures/blob.png");

//    sf::Sprite testblob(tex);

//    m_window.draw(testblob);

    sf::Sprite windowSprite(m_window.getTexture());
    windowSprite.setPosition(sf::Vector2f(
                                 GetAbsolutePosition().x,
                                 GetAbsolutePosition().y
                             ));

    rw.draw(windowSprite);

    m_titleBar.SetPosition(GetAbsolutePosition()); // Needs to be done here for smooth movement, should not cost much
    m_titleBar.Draw(rw);
}

void HWindow::DrawDebug(sf::RenderTarget& rw) {
    m_window.draw(m_debugShape);
}

void HWindow::SetPosition(const sf::Vector2f& newPosition) {
    m_absolutePosition = newPosition;

    m_debugShape.setPosition(0, 0);

    m_visualRect.top = m_absolutePosition.y;
    m_visualRect.left = m_absolutePosition.x;
}

} // namespace GUI
} // namespace HSF
