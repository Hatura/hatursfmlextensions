#pragma once

#include "hcontainer.h"

namespace HSF {
namespace GUI {

class HVerticalContainer : public HContainer {
  public:
    HVerticalContainer();

    void SetFillParentX();

    // Inherited from HComponent
    void Draw(sf::RenderTarget& rw) override;

private:
    // Inherited from HContainer
    void Layout(sf::Vector2f containerOffset) override;
};

} // namespace GUI
} // namespace HSF
