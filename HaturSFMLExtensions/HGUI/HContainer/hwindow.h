#pragma once

#include "hcontainer.h"
#include "HGUI/HUtility/hmouseinputreceiver.h"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include "HGUI/HUtility/htitlebar.h"

namespace HSF {
namespace GUI {

class HWindow : public HContainer
{
public:
    HWindow(unsigned int width, unsigned int height, const sf::Font& font);

    // Inherited from HComponent
    void Draw(sf::RenderTarget& rw) override;

    unsigned int GetWindowWidth() const;
    unsigned int GetWindowHeight() const;

    friend class HGUI;
    friend class HTitleBar;

private:
    HTitleBar               m_titleBar;

    sf::RenderTexture       m_window;
    sf::RectangleShape      m_windowBackground;

    unsigned int            m_drawIndex;

    // Inherited from HComponent
    void SetPosition(const sf::Vector2f& newPosition) override;
    void Layout(sf::Vector2f containerOffset) override;
    void DrawDebug(sf::RenderTarget& rw) override;

    // Inherited from HContainer
    bool DistributeMouseInput(const sf::Vector2f& mousePos, bool alreadySuccessfull) override;
    void DrawHierachy(sf::RenderTarget& rw, bool debugRender) override;

    // Inherited from HIMouseInputreceiver
//    void OnLeftMouseButtonDown() override;
};

} // namespace GUI
} // namespace HSF
