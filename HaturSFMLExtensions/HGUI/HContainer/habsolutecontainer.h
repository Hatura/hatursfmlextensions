#pragma once

#include "hcontainer.h"

namespace HSF {
namespace GUI {

class HAbsoluteContainer : public HContainer
{
public:
    HAbsoluteContainer();

    // Inherited from HComponent
    void Draw(sf::RenderTarget& rw) override;

private:
    // Inherited from HContainer
    void Layout(sf::Vector2f containerOffset) override;
};

} // namespace GUI
} // namespace HSF
