#include "hhorizontalcontainer.h"

#include "hverticalcontainer.h"

namespace HSF {
namespace GUI {

HHorizontalContainer::HHorizontalContainer() {

}

void HHorizontalContainer::SetFillParentY() {
    if (m_parentContainer != nullptr) {
        SetSize(sf::Vector2f(GetCalculatedSize().x, m_parentContainer->GetCalculatedSize().y));

        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HHorizontalContainer::Layout(sf::Vector2f containerOffset) {
    if (IsLayoutInvalidated()) {
        for (auto it = GetChildContainer().rbegin(); it != GetChildContainer().rend(); ++it) {
            sf::Vector2f siblingOffset(0.f, 0.f);

            if (it != GetChildContainer().rbegin()) {
                for (auto itx = GetChildContainer().rbegin(); itx != it; ++itx) {
                    siblingOffset += (*itx)->GetCalculatedSize();
                }
            }

            HVerticalContainer* isVContainer = dynamic_cast<HVerticalContainer*>(this);
            if (isVContainer != nullptr) {
                siblingOffset.x = 0.f;
            }

            HHorizontalContainer* isHContainer = dynamic_cast<HHorizontalContainer*>(this);
            if (isHContainer != nullptr) {
                siblingOffset.y = 0.f;
            }

            (*it)->Layout(GetCalculatedSize() + containerOffset - (*it)->GetCalculatedSize() - siblingOffset);
        }

        float highestComponent = 0.f;

        for (const auto& component : GetChildComponents()) {
            if (component->GetCalculatedSize().y > highestComponent) {
                highestComponent = component->GetCalculatedSize().y;
            }
        }

        if (GetCalculatedSize().y > highestComponent) {
            highestComponent = GetCalculatedSize().y;
        }

        float posX = 0.f;

        // Used to determine padding of prev Component
        HComponent* prevComponent = nullptr;

        for (auto& component : GetChildComponents()) {
            float posY = 0.f;
            if (component->GetVAlign() == EVerticalAlign::TOP) {
                posY = 0.f;
            } else if (component->GetVAlign() == EVerticalAlign::CENTER) {
                posY = highestComponent / 2.f - (component->GetCalculatedSize().y + component->GetMarginTop() + component->GetMarginBottom() + component->GetPaddingTop() + component->GetPaddingBottom()) / 2.f;
            } else if (component->GetVAlign() == EVerticalAlign::BOTTOM) {
                posY = highestComponent - (component->GetCalculatedSize().y + component->GetMarginTop() + component->GetMarginBottom() + component->GetPaddingTop() + component->GetPaddingBottom());
            }

            float widthDifference = 0.f;
            float heightDifference = 0.f;

            if (m_parentContainer != nullptr) {
                HContainer* pollingParent = this;
                HContainer* grandParent = m_parentContainer;

                if (grandParent != nullptr) {
                    while (true) {
                        HVerticalContainer* vContainer = dynamic_cast<HVerticalContainer*>(grandParent);
                        HHorizontalContainer* hContainer = dynamic_cast<HHorizontalContainer*>(grandParent);

                        if (vContainer != nullptr) {
                            widthDifference += grandParent->GetCalculatedSize().x - pollingParent->GetCalculatedSize().x;
                        }
                        else if (hContainer != nullptr) {
                            heightDifference += grandParent->GetCalculatedSize().y - pollingParent->GetCalculatedSize().y;
                        }

                        if (grandParent->m_parentContainer != nullptr) {
                            pollingParent = grandParent;
                            grandParent = pollingParent->m_parentContainer;
                        }
                        else {
                            break;
                        }
                    }
                }
            }

            // Margin
            // Horizontal
            float margX = 0.f;
            if (prevComponent != nullptr) {
                margX += prevComponent->m_marginRight;
            }
            margX += component->m_marginLeft;

            posX += margX;

            // Vertical
            float margY = 0.f;
            margY += component->m_marginTop;

            posY += margY;

            // Padding
            // Horizontal
            float padX = 0.f;
            if (prevComponent != nullptr) {
                padX += prevComponent->m_paddingRight;
            }
            padX += component->m_paddingLeft;

            posX += padX;

            // Vertical
            float padY = 0.f;
            padY += component->m_paddingTop;

            posY += padY;



            component->SetPosition(sf::Vector2f(posX + containerOffset.x - widthDifference, posY + containerOffset.y - heightDifference));

            component->OnLayout();

            posX += component->GetCalculatedSize().x;

            prevComponent = component.get();
        }
    }
}

void HHorizontalContainer::Draw(sf::RenderTarget& rw) {

}

} // namespace GUI
} // namespace HSF
