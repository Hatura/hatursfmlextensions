#pragma once

#include <memory>
#include <SFML/Graphics/Texture.hpp>

namespace HSF {
namespace GUI {

/**
 * @brief The HSimpleGraphicHolder class
 * Holds textures to hardcoded images, depending on the number of images it may be cost intensive and better to write
 * a system that returns a temporary but changes pointer later .. and load them in an extra thread
 */
class HSimpleGraphicHolder
{
public:
    HSimpleGraphicHolder();

    const sf::Texture* GetCloseButtonTexture() const;

private:
    std::unique_ptr<sf::Texture> m_closeButtonTex;

    void InitCloseButton();
};

} // namespace GUI
} // namespace HSF
