#include "hsimplegraphicholder.h"

#include <vector>
#include <SFML/Graphics/Image.hpp>

namespace HSF {
namespace GUI {

HSimpleGraphicHolder::HSimpleGraphicHolder()
    : m_closeButtonTex(nullptr) {

    InitCloseButton();
}

void HSimpleGraphicHolder::InitCloseButton() {
    sf::Image image;
    image.create(10, 10, sf::Color::Transparent);

    sf::Color xc(255, 0, 0, 255);

    using p = std::pair<int, int>;

    std::vector<std::pair<int, int>> closeButton {
        // border
        {p(0,0)},
        {p(0,1)}, {p(0,2)}, {p(0,3)}, {p(0,4)}, {p(0,5)}, {p(0,6)}, {p(0,7)}, {p(0,8)}, {p(0,9)},
        {p(1,0)}, {p(2,0)}, {p(3,0)}, {p(4,0)}, {p(5,0)}, {p(6,0)}, {p(7,0)}, {p(8,0)}, {p(9,0)},
        {p(9,1)}, {p(9,2)}, {p(9,3)}, {p(9,4)}, {p(9,5)}, {p(9,6)}, {p(9,7)}, {p(9,8)},
        {p(1,9)}, {p(2,9)}, {p(3,9)}, {p(4,9)}, {p(5,9)}, {p(6,9)}, {p(7,9)}, {p(8,9)}, {p(9,9)},
        //content
        {p(1,1)}, {p(2,2)}, {p(3,3)}, {p(4,4)}, {p(5,5)}, {p(6,6)}, {p(7,7)}, {p(8,8)},
        {p(8,1)}, {p(7,2)}, {p(6,3)}, {p(5,4)}, {p(4,5)}, {p(3,6)}, {p(2,7)}, {p(1,8)}
    };

    for (auto& coords : closeButton) {
        image.setPixel(coords.first, coords.second, xc);
    }

    m_closeButtonTex = std::unique_ptr<sf::Texture>(new sf::Texture());
    m_closeButtonTex->loadFromImage(image);
}

const sf::Texture* HSimpleGraphicHolder::GetCloseButtonTexture() const {
    return m_closeButtonTex.get();
}

} // namespace GUI
} // namespace HSF
