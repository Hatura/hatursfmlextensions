#pragma once

#include "HGUI/HUtility/hmouseinputreceiver.h"

#include <string>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "HGUI/HGFX/htextbutton.h"

namespace HSF {
namespace GUI {

class HComponent;
class HContainer;

/**
 * @brief The HTitleBar class
 *
 * Title bar, like a window uses one, can be moved
 *
 */

class HTitleBar {
public:
    HTitleBar(
        HComponent* associatedComponent,
        const sf::Font& font,
        const std::string& title = std::string("Untitled"),
        const sf::Color& backgroundColor = sf::Color(150, 150, 150, 255)
    );

    /**
     * @brief FeedPosition
     * Top-left of owner
     */
    void SetPosition(sf::Vector2f position);

    void AddCloseButton(HContainer* parentContainer, const sf::Font& font);
    void RemoveCloseButton();

    /**
     * @brief Draw
     * width of Component
     */
    void SetWidth(float width);

    bool PollMouseInputForButtons(bool alreadySuccessfull);
    void Draw(sf::RenderTarget& rw);

    HMouseInputReceiver<HTitleBar>* GetMouseInputReceiver() const;
    sf::FloatRect GetBoundingBox() const;

private:
    HComponent*                     m_associatedComponent;
    std::unique_ptr<HMouseInputReceiver<HTitleBar>> m_mouseInputReceiver;

    sf::RectangleShape              m_backgroundShape;
    sf::Vector2f                    m_size;
    std::string                     m_title;
    sf::Color                       m_backgroundColor;

    std::unique_ptr<HTextButton>    m_closeButton;
    bool                            m_buttonTextSizeCached;
    float                           m_cachedButtonTextSize;

//    void OnLeftMouseButtonDown() override;
//    void OnLeftMouseMove(const sf::Vector2f& relative, const sf::Vector2f& absolute) override;
};

} // namespace GUI
} // namespace HSF
