#include "htitlebar.h"

#include "HGUI/HContainer/hcontainer.h"
#include "HGUI/HContainer/hwindow.h"
#include "HGUI/hgui.h"
#include <iostream> // temp

namespace HSF {
namespace GUI {

HTitleBar::HTitleBar(HComponent* associatedComponent, const sf::Font& font, const std::string& title, const sf::Color& backgroundColor)
    : m_associatedComponent(associatedComponent)
    , m_mouseInputReceiver(nullptr)
    , m_backgroundShape()
    , m_size(0.f, 20.f)
    , m_title(title)
    , m_backgroundColor(backgroundColor)
    , m_closeButton(nullptr)
    , m_buttonTextSizeCached(false)
    , m_cachedButtonTextSize(0) {

    m_mouseInputReceiver = std::unique_ptr<HMouseInputReceiver<HTitleBar>>(new HMouseInputReceiver<HTitleBar>(this));

    m_backgroundShape.setFillColor(backgroundColor);

    m_mouseInputReceiver->AddLogicComponent(EMouseLogicComponent::LeftMove);
}

void HTitleBar::SetWidth(float width) {
    m_size.x = width;
    m_backgroundShape.setSize(m_size);
}

void HTitleBar::SetPosition(sf::Vector2f position) {
    position.y = position.y - m_size.y;
    m_backgroundShape.setPosition(position);

    if (m_closeButton != nullptr) {
        if (!m_buttonTextSizeCached) {
            m_cachedButtonTextSize = m_closeButton->GetTextSize().x;
            m_buttonTextSizeCached = true;
        }
        position.x = m_backgroundShape.getGlobalBounds().left + m_backgroundShape.getGlobalBounds().width - m_cachedButtonTextSize;
        m_closeButton->SetAbsolutePosition(position);
    }
}

void HTitleBar::AddCloseButton(HContainer* parentContainer, const sf::Font& font) {
    m_closeButton.reset(new HTextButton(std::string("x"), &font, 16));
    m_closeButton->RegisterCallback([this] (HComponent* self) {
        HWindow* window = dynamic_cast<HWindow*>(m_associatedComponent);
        if (window != nullptr) {
            window->GetHGUI()->RequestSafeRemoval(window);
        }
    });
}

void HTitleBar::RemoveCloseButton() {

}

bool HTitleBar::PollMouseInputForButtons(bool alreadySuccessfull) {
    if (!alreadySuccessfull) {
        alreadySuccessfull = m_closeButton->PollMouseInputExtern(
                               m_associatedComponent->GetHGUI()->GetCachedMousePosition(),
                               m_closeButton->GetVisualRect()
                           );
    }
    else {
        m_closeButton->ResetStatesExtern();
    }

    return alreadySuccessfull;
}

void HTitleBar::Draw(sf::RenderTarget& rw) {
    rw.draw(m_backgroundShape);

    if (m_closeButton != nullptr) {
        m_closeButton->Draw(rw);
    }
}

HSF::GUI::HMouseInputReceiver<HSF::GUI::HTitleBar>* HTitleBar::GetMouseInputReceiver() const {
    return m_mouseInputReceiver.get();
}

sf::FloatRect HTitleBar::GetBoundingBox() const {
    sf::FloatRect boundingBox = m_backgroundShape.getGlobalBounds();
    boundingBox.left = m_associatedComponent->GetAbsolutePosition().x;
    boundingBox.top = m_associatedComponent->GetAbsolutePosition().y - m_size.y;

    return boundingBox;
}

} // namespace GUI
} // namespace HSF
