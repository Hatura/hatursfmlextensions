#pragma once

#include <functional>
#include <vector>
#include <memory>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

namespace HSF {
namespace GUI {

bool PointInFloatRect(sf::Vector2f point, sf::FloatRect rect);

struct MouseData {
    MouseData()
        : m_prevPos(0.f, 0.f)
        , m_prevIntersected(false)
        , m_prevLeftDown(false)
        , m_prevRightDown(false)
        , m_leftMouseMoveStarted(false) { }

    sf::Vector2f    m_prevPos;
    bool            m_prevIntersected;
    bool            m_prevLeftDown;
    bool            m_prevRightDown;
    bool            m_leftMouseMoveStarted;
};

enum class EMouseLogicComponent {
    LeftMove,
    RightMove,
    LeftDragDrop,
    RightDragDrop
};

enum class EMouseButtonEvent {
    OnMouseEnter,
    OnMouseLeave,
    OnMouseOver,
    OnMouseLeftDown,
    OnMouseLeftUp,
    OnMouseLeftReset,
    OnMouseLeftMove,
    OnMouseRightDown,
    OnMouseRightUp,
    OnMouseRightReset
};

enum class EMouseMoveEvent {
    OnMouseLeftMove,
    OnMouseRightMove
};

template<class T>
class HMouseInputReceiver
{
public:
    HMouseInputReceiver(T* owner)
      : m_owner(owner)
      , m_leftMoveComponent(false)
      , m_rightMoveComponent(false)
      , m_leftDragDropComponent(false)
      , m_rightDragDropComponent(false) { }

    bool PollMouseInput(const sf::Vector2f& mousePos, const sf::FloatRect& rect) {
        bool isInVisualRect = PointInFloatRect(mousePos, rect);
        bool polled = false;

        if (isInVisualRect && !m_mouseData.m_prevIntersected) {
            OnMouseEnter();
            m_mouseData.m_prevIntersected = true;
            polled = true;
        } else if (!isInVisualRect && m_mouseData.m_prevIntersected) {
            OnMouseLeave();
            m_mouseData.m_prevIntersected = false;
            polled = true;
        }

        if (isInVisualRect) {
            OnMouseOver();
            polled = true;
        }

        if (m_leftMoveComponent && !sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
            m_mouseData.m_leftMouseMoveStarted = false;
        }

        if (m_leftMoveComponent && m_mouseData.m_leftMouseMoveStarted && sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
            OnLeftMouseMove(mousePos - m_mouseData.m_prevPos, mousePos);
            polled = true;
        }

        if (isInVisualRect && sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !m_mouseData.m_prevLeftDown) {
            OnLeftMouseButtonDown();
            m_mouseData.m_prevLeftDown = true;

            if (m_leftMoveComponent) {
                m_mouseData.m_leftMouseMoveStarted = true;
            }
            polled = true;
        } else if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && m_mouseData.m_prevLeftDown) {
            OnLeftMouseButtonUp();
            m_mouseData.m_prevLeftDown = false;
            polled = true;
        } else if (!isInVisualRect && sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && m_mouseData.m_prevLeftDown) {
            // Down but left rect
            OnLeftMouseButtonReset();
            m_mouseData.m_prevLeftDown = false;
            polled = true;
        }

        if (isInVisualRect && sf::Mouse::isButtonPressed(sf::Mouse::Button::Right) && !m_mouseData.m_prevRightDown) {
            OnRightMouseButtonDown();
            m_mouseData.m_prevRightDown = true;
            polled = true;
        } else if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Right) && m_mouseData.m_prevRightDown) {
            OnRightMouseButtonUp();
            m_mouseData.m_prevRightDown = false;
            polled = true;
        } else if (!isInVisualRect && sf::Mouse::isButtonPressed(sf::Mouse::Button::Right) && m_mouseData.m_prevRightDown) {
            // Down but left rect
            OnRightMouseButtonReset();
            m_mouseData.m_prevRightDown = false;
            polled = true;
        }

        m_mouseData.m_prevPos = mousePos;

        return polled;
    }

    void ResetStates() {
        // Needs to be called each tick for every Inputreceiver..
        if (m_mouseData.m_prevIntersected) {
//            if (m_mouseData.m_prevIntersected) {
                OnMouseLeave();
                m_mouseData.m_prevIntersected = false;
//            }
        }
    }

    void RegisterButtonFunction(EMouseButtonEvent event, std::function<void(T*)> functionObject) {
        std::unique_ptr<std::function<void(T* self)>> function(new std::function<void(T* self)>(std::move(functionObject)));
        switch (event) {
        case EMouseButtonEvent::OnMouseEnter:
            m_onMouseEnterF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseLeave:
            m_onMouseLeaveF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseOver:
            m_onMouseOverF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseLeftDown:
            m_onMouseLeftDownF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseLeftUp:
            m_onMouseLeftUpF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseLeftReset:
            m_onMouseLeftResetF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseRightDown:
            m_onMouseRightDownF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseRightUp:
            m_onMouseRightUpF.push_back(std::move(function));
            break;
        case EMouseButtonEvent::OnMouseRightReset:
            m_onMouseRightResetF.push_back(std::move(function));
            break;
        }
    }

    void RegisterMoveFunction(EMouseMoveEvent event, std::function<void(T*, const sf::Vector2f&, const sf::Vector2f&)> functionObject) {
        std::unique_ptr<std::function<void(T* self, const sf::Vector2f&, const sf::Vector2f&)>> function(new std::function<void(T* self, const sf::Vector2f&, const sf::Vector2f&)>(std::move(functionObject)));

        switch (event) {
        case EMouseMoveEvent::OnMouseLeftMove:
            m_onMouseLeftMoveF.push_back(std::move(function));
            break;
        }
    }

    void UnregisterFunctions(EMouseButtonEvent event) {
        switch (event) {
        case EMouseButtonEvent::OnMouseEnter:
            m_onMouseEnterF.clear();
            break;
        case EMouseButtonEvent::OnMouseLeave:
            m_onMouseLeaveF.clear();
            break;
        case EMouseButtonEvent::OnMouseOver:
            m_onMouseOverF.clear();
            break;
        case EMouseButtonEvent::OnMouseLeftDown:
            m_onMouseLeftDownF.clear();
            break;
        case EMouseButtonEvent::OnMouseLeftUp:
            m_onMouseLeftUpF.clear();
            break;
        case EMouseButtonEvent::OnMouseLeftReset:
            m_onMouseLeftResetF.clear();
            break;
        case EMouseButtonEvent::OnMouseRightDown:
            m_onMouseRightDownF.clear();
            break;
        case EMouseButtonEvent::OnMouseRightUp:
            m_onMouseRightUpF.clear();
            break;
        case EMouseButtonEvent::OnMouseRightReset:
            m_onMouseRightResetF.clear();
            break;
        }
    }

    void UnregisterAllEvents() {
        m_onMouseEnterF.clear();
        m_onMouseLeaveF.clear();
        m_onMouseOverF.clear();
        m_onMouseLeftDownF.clear();
        m_onMouseLeftUpF.clear();
        m_onMouseLeftResetF.clear();
        m_onMouseRightDownF.clear();
        m_onMouseRightUpF.clear();
        m_onMouseRightResetF.clear();

        m_linkedSelf = nullptr;
    }

    void AddLogicComponent(EMouseLogicComponent component) {
        switch (component) {
        case EMouseLogicComponent::LeftMove:
            m_leftMoveComponent = true;
            break;
        case EMouseLogicComponent::RightMove:
            m_rightMoveComponent = true;
            break;
        case EMouseLogicComponent::LeftDragDrop:
            m_leftDragDropComponent = true;
            break;
        case EMouseLogicComponent::RightDragDrop:
            m_rightDragDropComponent = true;
            break;
        }
    }

    void RemoveLogicComponent(EMouseLogicComponent component) {
        switch (component) {
        case EMouseLogicComponent::LeftMove:
            m_leftMoveComponent = false;
            break;
        case EMouseLogicComponent::RightMove:
            m_rightMoveComponent = false;
            break;
        case EMouseLogicComponent::LeftDragDrop:
            m_leftDragDropComponent = false;
            break;
        case EMouseLogicComponent::RightDragDrop:
            m_rightDragDropComponent = false;
            break;
        }
    }

private:
    T* m_owner;
    MouseData m_mouseData;

    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseEnterF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseLeaveF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseOverF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseLeftDownF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseLeftUpF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseLeftResetF;
    std::vector<std::unique_ptr<std::function<void(T* self, const sf::Vector2f&, const sf::Vector2f&)>>>  m_onMouseLeftMoveF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseRightDownF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseRightUpF;
    std::vector<std::unique_ptr<std::function<void(T* self)>>>  m_onMouseRightResetF;

    bool                m_leftMoveComponent;
    bool                m_rightMoveComponent;
    bool                m_leftDragDropComponent;
    bool                m_rightDragDropComponent;

    void OnMouseEnter() {
        if (m_onMouseEnterF.size() != 0) {
            for (auto& function : m_onMouseEnterF) {
                (*function)(m_owner);
            }
        }
    }

    void OnMouseLeave() {
        if (m_onMouseLeaveF.size() != 0) {
            for (auto& function : m_onMouseLeaveF) {
                (*function)(m_owner);
            }
        }
    }

    void OnMouseOver() {
        if (m_onMouseOverF.size() != 0) {
            for (auto& function : m_onMouseOverF) {
                (*function)(m_owner);
            }
        }
    }

    void OnLeftMouseButtonDown() {
        if (m_onMouseLeftDownF.size() != 0) {
            for (auto& function : m_onMouseLeftDownF) {
                (*function)(m_owner);
            }
        }
    }

    void OnLeftMouseButtonUp() {
        if (m_onMouseLeftUpF.size() != 0) {
            for (auto& function : m_onMouseLeftUpF) {
                (*function)(m_owner);
            }
        }
    }

    void OnLeftMouseButtonReset() {
        if (m_onMouseLeftResetF.size() != 0) {
            for (auto& function : m_onMouseLeftResetF) {
                (*function)(m_owner);
            }
        }
    }

    void OnRightMouseButtonDown() {
        if (m_onMouseRightDownF.size() != 0) {
            for (auto& function : m_onMouseRightDownF) {
                (*function)(m_owner);
            }
        }
    }

    void OnRightMouseButtonUp() {
        if (m_onMouseRightUpF.size() != 0) {
            for (auto& function : m_onMouseRightUpF) {
                (*function)(m_owner);
            }
        }
    }

    void OnRightMouseButtonReset() {
        if (m_onMouseRightResetF.size() != 0) {
            for (auto& function : m_onMouseRightResetF) {
                (*function)(m_owner);
            }
        }
    }

    /**
     * @brief OnLeftMove + OnRightMove
     * Called upon left button down + movement
     */
    void OnLeftMouseMove(const sf::Vector2f& relative, const sf::Vector2f& absolute) {
        if (m_onMouseLeftMoveF.size() != 0) {
            for (auto& function : m_onMouseLeftMoveF) {
                (*function)(m_owner, relative, absolute);
            }
        }
    }

//    virtual void OnRightMove(float absX, float absY);

//    virtual void OnLeftStartDrag();
//    virtual void OnLeftEndDrag(float x, float y, float magnitude);

//    virtual void OnRightStartDrag();
//    virtual void OnRightEndDrag(float x, float y, float magnitude);
};

} // namespace GUI
} // namespace HSF
