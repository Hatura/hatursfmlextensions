#include "hmouseinputreceiver.h"

namespace HSF {
namespace GUI {

bool PointInFloatRect(sf::Vector2f point, sf::FloatRect rect) {
    if (point.x >= rect.left
            && point.x <= rect.left + rect.width
            && point.y >= rect.top
            && point.y <= rect.top + rect.height) {
        return true;
    } else {
        return false;
    }
}

} // namespace GUI
} // namespace HSF
