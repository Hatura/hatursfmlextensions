#include "hcomponent.h"

#include "hgui.h"
#include "HContainer/habsolutecontainer.h"
#include "HContainer/hwindow.h"
#include <iostream>
#include <type_traits>

namespace HSF {
namespace GUI {

HComponent::HComponent()
    : m_parentContainer(nullptr)
    , m_hgui(nullptr)
    , m_size(0.f, 0.f)
    , m_absolutePosition(0.f, 0.f)
    , m_relativePosition(0.f, 0.f)
    , m_sizeOverwritable(false)
    , m_sizeOverwritten(false)
    , m_overwrittenSizeX(0.f)
    , m_overwrittenSizeY(0.f)
    , m_overwrittenRelativePosition(0.f, 0.f)
    , m_hAlign(EHorizontalAlign::LEFT)
    , m_vAlign(EVerticalAlign::TOP)
    , m_paddingTop(0.f)
    , m_paddingBottom(0.f)
    , m_paddingLeft(0.f)
    , m_paddingRight(0.f)
    , m_marginTop(0.f)
    , m_marginBottom(0.f)
    , m_marginLeft(0.f)
    , m_marginRight(0.f)
    , m_mouseInputReceiver(nullptr) {
    static unsigned int id = 0;
    m_uniqueID = id;
    ++id;

    m_debugShape.setFillColor(sf::Color::Transparent);
    m_debugShape.setOutlineThickness(1.0f);
    m_debugShape.setOutlineColor(sf::Color::Green);
}

unsigned int HComponent::GetUniqueID() const {
    return m_uniqueID;
}

void HComponent::OverwriteSize(float x, float y) {
    if (m_sizeOverwritable) {
        m_sizeOverwritten = true;
        m_overwrittenSizeX = x;
        m_overwrittenSizeY = y;

        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetSize(const sf::Vector2f& newSize) {
    m_size = newSize;

    m_debugShape.setSize(newSize);

    m_visualRect.width = m_size.x;
    m_visualRect.height = m_size.y;
}

void HComponent::SetPosition(const sf::Vector2f& newPosition) {
    m_absolutePosition = newPosition;

    m_debugShape.setPosition(newPosition);

    m_visualRect.top = m_absolutePosition.y;
    m_visualRect.left = m_absolutePosition.x;
}

void HComponent::CalculateRelativePosition() {
    if (m_parentContainer == nullptr) {
        m_relativePosition = m_absolutePosition;
    } else {
        m_relativePosition = m_absolutePosition - m_parentContainer->GetAbsolutePosition();
        std::cerr << m_relativePosition.x << " / " << m_relativePosition.y << std::endl;
    }
}

void HComponent::RequestSizeRecalculation() {
    if (m_parentContainer != nullptr) {
        HContainer* farthestParent = m_parentContainer;

        while (true) {
            if (farthestParent->m_parentContainer != nullptr) {
                farthestParent = farthestParent->m_parentContainer;
            } else {
                break;
            }
        }

        farthestParent->InvalidateHierachySize();
    } else {
        // No parent container
        OnCalculateSize();
    }
}

void HComponent::RequestLayoutInvalidation() {
    if (m_parentContainer != nullptr) {
        HContainer* farthestParent = m_parentContainer;

        while (true) {
            if (farthestParent->m_parentContainer != nullptr) {
                farthestParent = farthestParent->m_parentContainer;
            } else {
                break;
            }
        }

        farthestParent->InvalidateHierachyLayout();
    } else {
        // No parent container
        OnLayout();
        SetPosition(m_absolutePosition);
    }
}

void HComponent::SetSizeOverwritable(bool overwritable) {
    m_sizeOverwritable = overwritable;
}

void HComponent::AddMouseInputReceiver() {
    m_mouseInputReceiver.reset(new HMouseInputReceiver<HComponent>(this));
}

void HComponent::RemoveMouseInputReceiver() {
    m_mouseInputReceiver.reset();
}

HGUI* HComponent::GetHGUI() const {
    return m_hgui;
}

sf::Vector2f HComponent::GetOverwrittenRelativePosition() const {
    return m_overwrittenRelativePosition;
}

bool HComponent::HasMouseInputReceiver() const {
    return m_mouseInputReceiver != nullptr;
}

HMouseInputReceiver<HComponent>* HComponent::GetMouseInputReceiver() const {
    return m_mouseInputReceiver.get();
}

void HComponent::DrawDebug(sf::RenderTarget& rw) {
    rw.draw(m_debugShape);
}

void HComponent::SetHAlign(EHorizontalAlign hAlign) {
    m_hAlign = hAlign;
}

void HComponent::SetVAlign(EVerticalAlign vAlign) {
    m_vAlign = vAlign;
}

void HComponent::SetPadding(float top, float bottom, float left, float right) {
    m_paddingTop = top > 0.f ? top : 0.f;
    m_paddingBottom = bottom > 0.f ? bottom : 0.f;
    m_paddingLeft = left > 0.f ? left : 0.f;
    m_paddingRight = right > 0.f ? right : 0.f;

    // Maybe add another check and a new variable to indicate activation (being drawn), so this does not get called one million times.
    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetPadding(float amount) {
    amount = amount > 0.f ? amount : 0.f;

    m_paddingTop = amount;
    m_paddingBottom = amount;
    m_paddingLeft = amount;
    m_paddingRight = amount;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetPaddingTop(float amount) {
    m_paddingTop = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetPaddingBottom(float amount) {
    m_paddingBottom = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetPaddingLeft(float amount) {
    m_paddingLeft = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetPaddingRight(float amount) {
    m_paddingRight = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetMargin(float top, float bottom, float left, float right) {
    m_marginTop = top > 0.f ? top : 0.f;
    m_marginBottom = bottom > 0.f ? bottom : 0.f;
    m_marginLeft = left > 0.f ? left : 0.f;
    m_marginRight = right > 0.f ? right : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetMargin(float amount) {
    amount = amount > 0.f ? amount : 0.f;

    m_marginTop = amount;
    m_marginBottom = amount;
    m_marginLeft = amount;
    m_marginRight = amount;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetMarginTop(float amount) {
    m_marginTop = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetMarginBottom(float amount) {
    m_marginBottom = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetMarginLeft(float amount) {
    m_marginLeft = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetMarginRight(float amount) {
    m_marginRight = amount > 0.f ? amount : 0.f;

    if (m_parentContainer != nullptr) {
        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetAbsolutePosition(const sf::Vector2f& pos) {
    if (m_parentContainer == nullptr) {
        m_absolutePosition = pos;

        RequestSizeRecalculation();
        RequestLayoutInvalidation();
    }
}

void HComponent::SetAbsolutePosition(float x, float y) {
    SetAbsolutePosition(sf::Vector2f(x, y));
}

void HComponent::SetRelativePosition(const sf::Vector2f& pos) {
    if (m_parentContainer != nullptr) {
        HAbsoluteContainer* absConatiner = dynamic_cast<HAbsoluteContainer*>(m_parentContainer);
        HWindow* windowContainer = dynamic_cast<HWindow*>(m_parentContainer);

        if (absConatiner != nullptr) {
            m_overwrittenRelativePosition = pos;

            m_debugShape.setPosition(absConatiner->GetAbsolutePosition() + m_overwrittenRelativePosition);

            RequestSizeRecalculation();
            RequestLayoutInvalidation();
        } else if (windowContainer != nullptr) {
            m_overwrittenRelativePosition = pos;

            m_debugShape.setPosition(windowContainer->GetAbsolutePosition() + m_overwrittenRelativePosition);

            RequestSizeRecalculation();
            RequestLayoutInvalidation();
        }
    }
}

void HComponent::SetRelativePosition(float x, float y) {
    SetRelativePosition(sf::Vector2f(x, y));
}

bool HComponent::Compare(const HComponent* other) const {
    return GetUniqueID() == other->GetUniqueID();
}

const sf::FloatRect& HComponent::GetVisualRect() const {
    return m_visualRect;
}

HSF::GUI::EVerticalAlign HComponent::GetVAlign() const {
    return m_vAlign;
}

HSF::GUI::EHorizontalAlign HComponent::GetHAlign() const {
    return m_hAlign;
}

float HComponent::GetMarginTop() const {
    return m_marginTop;
}

float HComponent::GetMarginBottom() const {
    return m_marginBottom;
}

float HComponent::GetMarginLeft() const {
    return m_marginLeft;
}

float HComponent::GetMarginRight() const {
    return m_marginRight;
}

float HComponent::GetPaddingTop() const {
    return m_paddingTop;
}

float HComponent::GetPaddingBottom() const {
    return m_paddingBottom;
}

float HComponent::GetPaddingLeft() const {
    return m_paddingLeft;
}

float HComponent::GetPaddingRight() const {
    return m_paddingRight;
}

bool HComponent::IsSizeOverwritten() const {
    return m_sizeOverwritten;
}

float HComponent::GetOverwrittenSizeX() const {
    return m_overwrittenSizeX;
}

float HComponent::GetOverwrittenSizeY() const {
    return m_overwrittenSizeY;
}

sf::Vector2f HComponent::GetAbsolutePosition() const {
    return m_absolutePosition;
}

sf::Vector2f HComponent::GetRelativePosition() const {
    return m_relativePosition;
}

sf::Vector2f HComponent::GetCalculatedSize() const {
    return m_size;
}

bool HComponent::HasParent() const {
    return m_parentContainer != nullptr;
}

HSF::GUI::HContainer* HComponent::GetParent() const {
    return m_parentContainer;
}

} // namespace GUI
} // namespace HSF
