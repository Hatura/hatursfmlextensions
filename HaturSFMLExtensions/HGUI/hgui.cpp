#include "hgui.h"

#include <iostream>
#include <SFML/Window/Mouse.hpp>
#include "HUtility/hmouseinputreceiver.h"
#include "HContainer/hwindow.h"

#include <iostream> // test

namespace HSF {
namespace GUI {

HGUI::HGUI(sf::RenderWindow& rw, EResizeBehavior resizeBehavior)
    : m_rw(rw)
    , m_resizeBehavior(resizeBehavior)
    , m_view(sf::FloatRect(0, 0, rw.getSize().x, rw.getSize().y))
    , m_sgh(nullptr)
    , m_debugModeEnabled(false)
    , m_lastCachedMousePosition(0.f, 0.f)
    , m_rearrangeWindows(false)
    , m_requestedFrontWindow(nullptr) {

}

void HGUI::Draw() {
    const sf::View& savedView = m_rw.getView();

    m_rw.setView(m_view);

//    sf::Vector2f mousePosAbs = sf::Vector2f(
//                                   static_cast<float>(sf::Mouse::getPosition(m_rw).x),
//                                   static_cast<float>(sf::Mouse::getPosition(m_rw).y)
//                               );

    sf::Vector2f mousePosRel = m_rw.mapPixelToCoords(sf::Mouse::getPosition(m_rw), m_view);

    m_lastCachedMousePosition = mousePosRel;

    std::vector<sf::Mouse::Button> buttons = GetPressedMouseButtons();

    RemoveFlaggedContainers(); // Safely remove for deletion flagged containers

    if (m_rearrangeWindows) {
        ReArrangeWindows();
    }

    bool mouseInputPolled = false;

    for (auto it = m_windowChildrenPtr.rbegin(); it != m_windowChildrenPtr.rend(); ++it) {
        if (m_rw.hasFocus() && !mouseInputPolled) {
            if ((*it)->DistributeMouseInput(mousePosRel, false)) {
                mouseInputPolled = true;
            }
        }
    }

    for (auto& container : m_containerChildren) {
        container->RecalculateHierachy();
        container->Layout(container->GetAbsolutePosition());
        container->PostLayout();
        if (m_rw.hasFocus() && !mouseInputPolled) {
            if (container->DistributeMouseInput(mousePosRel, false)) {
                mouseInputPolled = true;
            }
        }
        container->DrawHierachy(m_rw, m_debugModeEnabled);
    }

    for (auto& window : m_windowChildrenPtr) {
        window->RecalculateHierachy();
        window->Layout(window->GetAbsolutePosition());
        window->PostLayout();
        window->DrawHierachy(m_rw, m_debugModeEnabled);
    }

    m_rw.setView(savedView);
}

void HGUI::OnWindowResize() {
//    for (auto& container : m_containerChildren) {
//        container->InvalidateHierachySize();
//        container->InvalidateHierachyLayout();

//        std::cerr << container->GetAbsolutePosition().x << " | " << container->GetAbsolutePosition().y << std::endl;
//    }

    if (m_resizeBehavior == EResizeBehavior::ExpandOnResize) {
        m_view.setSize(m_rw.getSize().x, m_rw.getSize().y);
        m_view.setCenter(static_cast<float>(m_rw.getSize().x) / 2.f, static_cast<float>(m_rw.getSize().y) / 2.f);
    } else if (m_resizeBehavior == EResizeBehavior::StretchOnResize) {

    }

//    for (auto& window : m_windowChildren) {
//        window->InvalidateHierachySize();
//        window->InvalidateHierachyLayout();
//    }
}

void HGUI::RequestSafeRemoval(HContainer* container) {
    m_deletionFlaggedContainers.insert(container);
}

void HGUI::RemoveFlaggedContainers() {
    if (m_deletionFlaggedContainers.size() > 0) {
        size_t totalFlaggedComponentCount = m_deletionFlaggedContainers.size();

        for (auto& container : m_deletionFlaggedContainers) {
            bool containerRemovalSuccess = false;

            HWindow* window = dynamic_cast<HWindow*>(container);
            if (window != nullptr) {
                // remove from m_windowChildren
                auto windowDeleterIT = m_windowChildren.end();

                for (auto it = m_windowChildren.begin(); it != m_windowChildren.end(); ++it) {
                    if ((*it)->Compare(window)) {
                        windowDeleterIT = it;
                    }
                }

                if (windowDeleterIT != m_windowChildren.end()) {
                    (*windowDeleterIT)->RemoveAllComponents();

                    m_windowChildren.erase(windowDeleterIT);

                    containerRemovalSuccess = true;

                    std::cerr << "removed window" << std::endl;
                }

                // remove from m_windowChildrenPtr
                auto windowPtrDeleterIT = m_windowChildrenPtr.end();

                for (auto it = m_windowChildrenPtr.begin(); it != m_windowChildrenPtr.end(); ++it) {
                    if ((*it)->Compare(window)) {
                        windowPtrDeleterIT = it;
                    }
                }

                if (windowPtrDeleterIT != m_windowChildrenPtr.end()) {
                    m_windowChildrenPtr.erase(windowPtrDeleterIT);

                    std::cerr << "removed window ptr" << std::endl;
                }
            } else {
                auto containerDeleterIT = m_containerChildren.end();

                for (auto it = m_containerChildren.begin(); it != m_containerChildren.end(); ++it) {
                    if ((*it)->Compare(container)) {
                        containerDeleterIT = it;
                    }
                }

                if (containerDeleterIT != m_containerChildren.end()) {
                    (*containerDeleterIT)->RemoveAllComponents();

                    m_containerChildren.erase(containerDeleterIT);

                    containerRemovalSuccess = true;
                }
            }

            if (containerRemovalSuccess) {
                --totalFlaggedComponentCount;
            }
        }

        if (totalFlaggedComponentCount > 0) {
            std::cerr << "Couldn't safely delete " << totalFlaggedComponentCount << " components in RemoveFlaggedContainers() hgui.cpp" << std::endl;
        }
        m_deletionFlaggedContainers.clear();
    }
}

void HGUI::ReArrangeWindows() {
    unsigned int i = 0;

    for (auto& window : m_windowChildren) {
        if (window->Compare(m_requestedFrontWindow)) {
            window->m_drawIndex = m_windowChildren.size();
        } else {
            window->m_drawIndex = i;
            ++i;
        }
    }

    std::sort(m_windowChildrenPtr.begin(), m_windowChildrenPtr.end(),
    [] (HWindow* windowA, HWindow* windowB) {
        return windowA->m_drawIndex < windowB->m_drawIndex;
    });

    m_rearrangeWindows = false;
    m_requestedFrontWindow = nullptr;
}

void HGUI::BringWindowToFront(HWindow* window) {
    if (m_windowChildrenPtr.back()->Compare(window)) {
        // Already front
        return;
    }

    m_rearrangeWindows = true;
    m_requestedFrontWindow = window;
}

//void HGUI::AddDefaultFont(const std::string& fontFile) {
//    m_defaultFont.reset(std::unique_ptr<sf::Font>(new sf::Font()));
//    m_defaultFont->loadFromFile(fontFile);
//    m_defaultFontPtr = m_defaultFont.get();
//}

//void HGUI::AddDefaultFont(const sf::Font* font) {
//    m_defaultFontPtr = font;
//}

void HGUI::SetDebugMode(bool debug) {
    m_debugModeEnabled = debug;
}

const HSF::GUI::HSimpleGraphicHolder* HGUI::GetSimpleGraphicsHolder() const {
    return m_sgh.get();
}

size_t HGUI::GetNumWindows() const {
    return m_windowChildren.size();
}

size_t HGUI::GetNumNonWindowContainers() const {
    return m_containerChildren.size();
}

size_t HGUI::GetNumTotalContainers() const {
    return m_windowChildren.size() + m_containerChildren.size();
}

const sf::RenderWindow& HGUI::GetRenderWindow() const {
    return m_rw;
}

const sf::View& HGUI::GetGUIView() const {
    return m_view;
}

const sf::Vector2f& HGUI::GetCachedMousePosition() const {
    return m_lastCachedMousePosition;
}

std::vector<sf::Mouse::Button> HGUI::GetPressedMouseButtons() {
    std::vector<sf::Mouse::Button> buttons;

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
        buttons.push_back(sf::Mouse::Left);
    }

    if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
        buttons.push_back(sf::Mouse::Right);
    }

    if (sf::Mouse::isButtonPressed(sf::Mouse::Middle)) {
        buttons.push_back(sf::Mouse::Middle);
    }

    return buttons;
}

} // namespace GUI
} // namespace HSF
