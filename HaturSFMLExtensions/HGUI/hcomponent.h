#pragma once

#include <vector>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "HUtility/hmouseinputreceiver.h"

namespace HSF {
namespace GUI {

class HGUI;
class HContainer;

enum class EVerticalAlign {
    TOP,
    CENTER,
    BOTTOM
};

enum class EHorizontalAlign {
    LEFT,
    CENTER,
    RIGHT
};

/**
 * @brief The HComponent class
 *
 * Pure virtual methods:
 *
 * Draw() - Draw component innerts
 * CalculateSize() - Use Setsize to determine widget size
 * OnLayout() - position subelements relative to computed AbsolutePosition
 */
class HComponent
{
public:
    HComponent();
    virtual ~HComponent() { }

    unsigned int GetUniqueID() const;

    // Virtual methods
    /**
     * @brief OverwriteSize
     * Sets m_overwrittenSizeX and m_overwrittenSizeY, if overwriting the size is a legitimate approach the new
     * sizes must be handled in the widget, this does nothing if not implemented
     */
    virtual void OverwriteSize(float x, float y);

    /**
     * @brief SetHAlign
     * Only components in a HVerticalContainer can have HorizontalAlignment!
     */
    void SetHAlign(EHorizontalAlign hAlign);

    /**
     * @brief SetVAlign
     * Only components in a HHorizontalContainer can have VerticalAlignment!
     */
    void SetVAlign(EVerticalAlign vAlign);

    /**
     * @brief SetPadding & SetMargin
     *
     * ##############################
     * #                Margin  |
     * #     ___________________|____
     * #    |border     Padding |
     * #    |                   |
     * #    |   #####################
     * #    |   #                   ^
     * #    |   #                   |
     * #    |   #   width x height  |
     * #    |   # <------------------
     */
    void SetPadding(float top, float bottom, float left, float right);
    void SetPadding(float amount);
    void SetPaddingTop(float amount);
    void SetPaddingBottom(float amount);
    void SetPaddingLeft(float amount);
    void SetPaddingRight(float amount);

    void SetMargin(float top, float bottom, float left, float right);
    void SetMargin(float amount);
    void SetMarginTop(float amount);
    void SetMarginBottom(float amount);
    void SetMarginLeft(float amount);
    void SetMarginRight(float amount);

    /**
     * @brief SetAbsolutePosition
     * Sets the absolute position for containers but only if it's the first in hierachy HGui->FirstLayer
     */
    void SetAbsolutePosition(const sf::Vector2f& pos);
    void SetAbsolutePosition(float x, float y);

    /**
     * @brief SetRelativePosition
     * Sets the relative position, only works if base is of base or derived HAbsoluteContainer or HWindow
     */
    void SetRelativePosition(const sf::Vector2f& pos);
    void SetRelativePosition(float x, float y);

    virtual void Draw(sf::RenderTarget& rw) = 0;

    /**
     * @brief Compare
     * Use this for virtual subclasses, because operator == does not support it.
     */
    bool Compare(const HComponent* other) const;

    const sf::FloatRect& GetVisualRect() const;

    EVerticalAlign GetVAlign() const;
    EHorizontalAlign GetHAlign() const;

    float GetMarginTop() const;
    float GetMarginBottom() const;
    float GetMarginLeft() const;
    float GetMarginRight() const;

    float GetPaddingTop() const;
    float GetPaddingBottom() const;
    float GetPaddingLeft() const;
    float GetPaddingRight() const;

    bool IsSizeOverwritten() const;
    float GetOverwrittenSizeX() const;
    float GetOverwrittenSizeY() const;

    /**
     * @brief GetCalculatedAbsPosition
     * The calculated (or manually set for root/AbsoluteContainer) position of the container/widget
     */
    sf::Vector2f GetAbsolutePosition() const;

    /**
     * @brief GetRelativePosition
     * The position relative to its parent
     */
    sf::Vector2f GetRelativePosition() const;

    /**
     * @brief GetCalculatedSize
     * Size of Widgets is set by them, size of containers is auto-calculated
     */
    sf::Vector2f GetCalculatedSize() const;

    /**
     * @brief GetHGUI
     * Would be good if this could be const..
     */
    HGUI* GetHGUI() const;

    bool HasParent() const;
    HContainer* GetParent() const;

    /**
     * Gives the container subclasses access to protected and private member functions and variables, they need
     * it because they iterate over child containers and making the functions public would expose them for
     * external usage.. this improves encapsulation.
     */
    friend class HContainer;
    friend class HVerticalContainer;
    friend class HHorizontalContainer;
    friend class HAbsoluteContainer;
    friend class HWindow;

protected:
    /**
     * @brief RequestSizeRecalculation & RequestLayoutInvalidation
     *
     * Internally called when something with the layout is changed, you can also call this from outside but it is expensive,
     * also never call LayoutHierachy oder CalculateHierachy directly!
     */
    void RequestSizeRecalculation();
    void RequestLayoutInvalidation();

    /**
     * @brief SetSize
     * SetSize is called within AddComponent of HContainer to calculate container sizes, sizes of widgets are manually
     * set in their respective classes
     */
    void SetSize(const sf::Vector2f& newSize);

    /**
     * @brief SetPosition
     * SetPosition is called by the Containers Layout() to set the absolute position (reminder: It's prohibited in
     * the public Method SetAbsolutePosition if the parent is not root (nullptr) oder a AbsoluteContainer)
     */
    virtual void SetPosition(const sf::Vector2f& newPosition);

    void SetSizeOverwritable(bool overwritable);

    virtual void AddMouseInputReceiver();
    void RemoveMouseInputReceiver();

    bool HasMouseInputReceiver() const;
    HMouseInputReceiver<HComponent>* GetMouseInputReceiver() const;

    sf::Vector2f GetOverwrittenRelativePosition() const;

private:
    // Pure virtual methods
    /**
     * @brief CalculateSize
     * Overwrite this and set the resize guidelines for the widget.. it's called in Containers RecalculateContainerSizes()
     */
    virtual void OnCalculateSize() = 0;
    virtual void OnLayout() = 0;

    virtual void DrawDebug(sf::RenderTarget& rw);

    void CalculateRelativePosition();

    HGUI* m_hgui;

    HContainer* m_parentContainer;
    unsigned int m_uniqueID;

    sf::Vector2f m_absolutePosition;
    sf::Vector2f m_relativePosition;
    sf::Vector2f m_size;

    sf::RectangleShape m_debugShape;

    sf::FloatRect m_visualRect;

    bool m_sizeOverwritable;
    bool m_sizeOverwritten;
    float m_overwrittenSizeX;
    float m_overwrittenSizeY;

    sf::Vector2f m_overwrittenRelativePosition;

    EHorizontalAlign m_hAlign;
    EVerticalAlign m_vAlign;

    float m_paddingTop;
    float m_paddingBottom;
    float m_paddingLeft;
    float m_paddingRight;

    float m_marginTop;
    float m_marginBottom;
    float m_marginLeft;
    float m_marginRight;

    bool m_hasMouseInputReceiver;
    std::unique_ptr<HMouseInputReceiver<HComponent>> m_mouseInputReceiver;
};

} // namespace GUI
} // namespace HSF
