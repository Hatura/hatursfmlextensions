#include "htext.h"

#include "HGUI/HContainer/hcontainer.h"
#include <iostream>

namespace HSF {
namespace GUI {

sf::Vector2f CalculateTextSize(const sf::Text& text) {
    const sf::String str = text.getString() + '\n';

    float maxLineWidth = 0.f;
    float lineWidth = 0.f;
    unsigned int lines = 0;

    for (sf::String::ConstIterator itr = str.begin(); itr != str.end(); ++itr) {
        if (*itr == '\n') {
            ++lines;
            maxLineWidth = std::max(maxLineWidth, lineWidth);
            lineWidth = 0.f;
        } else {
            lineWidth += text.getFont()->getGlyph(*itr, text.getCharacterSize(), text.getStyle() & sf::Text::Bold).advance;
        }
    }

    const float lineHeight = static_cast<float>(text.getFont()->getLineSpacing(text.getCharacterSize()));
    return sf::Vector2f(maxLineWidth, lines * lineHeight);
}

HText::HText(sf::Text* textPtr, const sf::Color& backgroundColor)
    : m_text(nullptr)
    , m_textPtr(textPtr) {

    HTextPostInit(backgroundColor);
}

HText::HText(const std::string& textString, const sf::Font* font, unsigned int characterSize, const sf::Color& textColor, const sf::Color& backgroundColor) {
    m_text = std::unique_ptr<sf::Text>(new sf::Text);
    m_text->setFont(*font);
    m_text->setCharacterSize(characterSize);
    m_text->setColor(textColor);
    m_text->setString(textString);

    m_textPtr = m_text.get();

    HTextPostInit(backgroundColor);
}

void HText::HTextPostInit(const sf::Color& backgroundColor) {
    SetSizeOverwritable(true);

    m_backgroundColor = backgroundColor;

    m_backgroundShape.setFillColor(m_backgroundColor);
}

void HText::OnCalculateSize() {
    sf::Vector2f textSize = CalculateTextSize(*m_textPtr);

    if (IsSizeOverwritten()) {
        SetSize(sf::Vector2f(GetOverwrittenSizeX(), GetOverwrittenSizeY()));
        m_backgroundShape.setSize(sf::Vector2f(GetOverwrittenSizeX(), GetOverwrittenSizeY()));
    } else {
        SetSize(textSize);
        m_backgroundShape.setSize(textSize);
    }
}

void HText::OnLayout() {
    if (IsSizeOverwritten()) {
        sf::Vector2f textSize = CalculateTextSize(*m_textPtr);

        m_textPtr->setPosition(sf::Vector2f(
                                   GetAbsolutePosition().x + GetCalculatedSize().x / 2.f - textSize.x / 2.f,
                                   GetAbsolutePosition().y + GetCalculatedSize().y / 2.f - textSize.y / 2.f
                               ));
        m_backgroundShape.setPosition(GetAbsolutePosition());
    } else {
        m_textPtr->setPosition(GetAbsolutePosition());
        m_backgroundShape.setPosition(GetAbsolutePosition());
    }
}

void HText::Draw(sf::RenderTarget& rw) {
    rw.draw(m_backgroundShape);
    rw.draw(*m_textPtr);
}

const sf::Vector2f HText::GetTextSize() const {
    return CalculateTextSize(*m_text);
}

//const sf::Text& HText::GetText() const {
//    return (*m_text);
//}

const sf::Color& HText::GetBackgroundColor() const {
    return m_backgroundColor;
}

sf::RectangleShape& HText::GetBackgroundShape() {
    return m_backgroundShape;
}

} // namespace GUI
} // namespace HSF
