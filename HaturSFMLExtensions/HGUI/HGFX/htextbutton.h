#pragma once

#include "htext.h"
#include "HGUI/HInterface/hbuttoninterface.h"

#include <functional>
#include "HGUI/HUtility/hmouseinputreceiver.h"

namespace HSF {
namespace GUI {

class HTextButton : public HText, public HButtonInterface
{
public:
    explicit HTextButton(
        sf::Text* textPtr,
        const sf::Color& backgroundColor = sf::Color(112, 112, 112, 255),
        const sf::Color& mouseOverColor = sf::Color(90, 90, 90, 255),
        const sf::Color& mouseDownColor = sf::Color(70, 70, 70, 255)
    );

    HTextButton(const std::string& textString,
        const sf::Font* font,
        unsigned int textSize = 26,
        const sf::Color& textColor = sf::Color::White,
        const sf::Color& backgroundColor = sf::Color(110, 110, 110, 255),
        const sf::Color& mouseOverColor = sf::Color(90, 90, 90, 255),
        const sf::Color& mouseDownColor = sf::Color(70, 70, 70, 255)
    );

    const sf::Color& GetMouseOverColor() const;
    const sf::Color& GetMouseDownColor() const;

    /**
     * @brief RegisterCallback
     * For left click or enter
     */
    void RegisterCallback(std::function<void(HComponent*)> functionObject) override;

    /**
     * @brief RegisterAltCallback
     * For right click
     */
    void RegisterAltCallback(std::function<void(HComponent*)> functionObject) override;

    bool PollMouseInputExtern(const sf::Vector2f& mousePos, const sf::FloatRect& rect);
    void ResetStatesExtern();

private:
    sf::Color               m_mouseOverColor;
    sf::Color               m_mouseDownColor;

    void HTextButtonPostInit(const sf::Color& mouseOverColor, const sf::Color& mouseDownColor);

//    void OnMouseEnter() override;
//    void OnMouseOver() override;
//    void OnMouseLeave() override;
//    void OnLeftMouseButtonDown() override;
//    void OnLeftMouseButtonUp() override;
//    void OnRightMouseButtonDown() override;
//    void OnRightMouseButtonUp() override;
};

} // namespace HSF
} // namespace GUI
