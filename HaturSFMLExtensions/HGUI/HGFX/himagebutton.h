#pragma once

#include "himage.h"
#include "htext.h"
#include "HGUI/HInterface/hbuttoninterface.h"

namespace HSF {
namespace GUI {

/**
 * @brief The HImageButton class
 *
 * HImages m_texture / m_texturePtr / m_sprite represents HImageButton's textureUp
 */
class HImageButton : public HImage, public HButtonInterface
{
public:
    explicit HImageButton(const sf::Texture* textureUp, const sf::Texture* textureDown = nullptr, const sf::Texture* textureHover = nullptr);
    explicit HImageButton(const std::string& texUpFilename, const std::string& texDownFilename = std::string("UNDEFINED"), const std::string& texHoverFilename = std::string("UNDEFINED"));
    explicit HImageButton(std::shared_ptr<sf::Texture> sharedUp, std::shared_ptr<sf::Texture> sharedDown = nullptr, std::shared_ptr<sf::Texture> sharedHover = nullptr);

    void AddText(const std::string& text, const sf::Font* font, const sf::Color& fontColor, unsigned int characterSize = 16);

    /**
     * @brief FitToText
     * @param autoPadding = Adds 10% of size as padding (for borders)
     * @param symmetrical = Expands to fit texture sizes
     */
    void FitToText(bool symmetrical = false, float hPadding = 0.f, float vPadding = 0.f);
    void FitToTexture();

    void Draw(sf::RenderTarget& rw) override;

    void RegisterCallback(std::function<void(HComponent*)> functionObject) override;
    void RegisterAltCallback(std::function<void(HComponent *)> functionObject) override;

    bool IsFitToText() const;

protected:
    void OnCalculateSize() override;
    void OnLayout() override;

private:
    void HImageButtonPostInit();

    EButtonState                    m_buttonState;

    std::shared_ptr<sf::Texture>    m_textureDown;
    mutable const sf::Texture*      m_textureDownPtr;
    std::unique_ptr<sf::Sprite>     m_spriteDown;

    std::shared_ptr<sf::Texture>    m_textureHover;
    mutable const sf::Texture*      m_textureHoverPtr;
    std::unique_ptr<sf::Sprite>     m_spriteHover;

    std::unique_ptr<HText>          m_text;

    bool                            m_fitToText;
    bool                            m_fitToTextSymmetrical;
    float                           m_fitToTextHPadding;
    float                           m_fitToTextVPadding;
};

} // namespace GUI
} // namespace HSF
