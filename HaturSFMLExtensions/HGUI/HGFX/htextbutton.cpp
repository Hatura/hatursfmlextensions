#include "htextbutton.h"

#include <iostream> // test

namespace HSF {
namespace GUI {

HTextButton::HTextButton(sf::Text* textPtr, const sf::Color& backgroundColor, const sf::Color& mouseOverColor, const sf::Color& mouseDownColor)
    : HText(textPtr, backgroundColor) {

    HTextButtonPostInit(mouseOverColor, mouseDownColor);
}

HTextButton::HTextButton(const std::string& textString, const sf::Font* font, unsigned int textSize, const sf::Color& textColor, const sf::Color& backgroundColor, const sf::Color& mouseOverColor, const sf::Color& mouseDownColor)
    : HText(textString, font, textSize, textColor, backgroundColor) {

    HTextButtonPostInit(mouseOverColor, mouseDownColor);
}

const sf::Color& HTextButton::GetMouseOverColor() const {
    return m_mouseOverColor;
}

const sf::Color& HTextButton::GetMouseDownColor() const {
    return m_mouseDownColor;
}

void HTextButton::RegisterCallback(std::function<void(HComponent*)> functionObject) {
    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftUp, functionObject);
}

void HTextButton::RegisterAltCallback(std::function<void(HComponent*)> functionObject) {
    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseRightUp, std::move(functionObject));
}

bool HTextButton::PollMouseInputExtern(const sf::Vector2f& mousePos, const sf::FloatRect& rect) {
    return GetMouseInputReceiver()->PollMouseInput(mousePos, rect);
}

void HTextButton::ResetStatesExtern() {
    GetMouseInputReceiver()->ResetStates();
}

void HTextButton::HTextButtonPostInit(const sf::Color& mouseOverColor, const sf::Color& mouseDownColor) {
    m_mouseOverColor = mouseOverColor;
    m_mouseDownColor = mouseDownColor;

    AddMouseInputReceiver();

    // Default mouse behavior
    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseEnter, [this] (HComponent* self) {
        GetBackgroundShape().setFillColor(m_mouseOverColor);
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeave, [this] (HComponent* self) {
        GetBackgroundShape().setFillColor(GetBackgroundColor());
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftDown, [this] (HComponent* self) {
        GetBackgroundShape().setFillColor(m_mouseDownColor);
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftUp, [this] (HComponent* self) {
        GetBackgroundShape().setFillColor(GetBackgroundColor());
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftReset, [this] (HComponent* self) {
        GetBackgroundShape().setFillColor(GetBackgroundColor());
    });
}

} // namespace GUI
} // namespace HSF
