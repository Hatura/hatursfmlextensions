#include "himage.h"

namespace HSF {
namespace GUI {

HImage::HImage(const sf::Texture* texturePtr)
    : m_texture(nullptr)
    , m_texturePtr(texturePtr)
    , m_sprite(nullptr) {

    HImagePostInit();
}

HImage::HImage(const std::string& fileName)
    : m_sprite(nullptr) {
    m_texture = std::shared_ptr<sf::Texture>(new sf::Texture());
    m_texture->loadFromFile(fileName);
    m_texturePtr = m_texture.get();

    HImagePostInit();
}

HImage::HImage(std::shared_ptr<sf::Texture> sharedTexture)
    : m_texture(sharedTexture)
    , m_texturePtr(sharedTexture.get())
    , m_sprite(nullptr) {

    HImagePostInit();
}

void HImage::HImagePostInit() {
    m_sprite = std::unique_ptr<sf::Sprite>(new sf::Sprite(*m_texturePtr));
}

void HImage::OnCalculateSize() {
    SetSize(sf::Vector2f(m_sprite->getLocalBounds().width, m_sprite->getLocalBounds().height));
}

void HImage::OnLayout() {
    m_sprite->setPosition(GetAbsolutePosition());
}

void HImage::Draw(sf::RenderTarget& rw) {
    rw.draw(*m_sprite);
}

sf::Sprite* HImage::GetSprite() const {
    return m_sprite.get();
}

const sf::Texture* HImage::GetTexture() const {
    return m_texturePtr;
}

}
}
