#include "himagebutton.h"

namespace HSF {
namespace GUI {

HImageButton::HImageButton(const sf::Texture* textureUp, const sf::Texture* textureDown, const sf::Texture* textureHover)
    : HImage(textureUp)
    , m_textureDown(nullptr)
    , m_textureDownPtr(textureDown)
    , m_textureHover(nullptr)
    , m_textureHoverPtr(textureHover)
    , m_spriteDown(nullptr)
    , m_spriteHover(nullptr)
    , m_buttonState(EButtonState::UP) {

    HImageButtonPostInit();
}

HImageButton::HImageButton(const std::string& texUpFilename, const std::string& texDownFilename, const std::string& texHoverFilename)
    : HImage(texUpFilename)
    , m_textureDown(nullptr)
    , m_textureDownPtr(nullptr)
    , m_textureHover(nullptr)
    , m_textureHoverPtr(nullptr)
    , m_spriteDown(nullptr)
    , m_spriteHover(nullptr)
    , m_buttonState(EButtonState::UP) {

    if (texDownFilename != std::string("UNDEFINED")) {
        m_textureDown = std::shared_ptr<sf::Texture>(new sf::Texture());
        m_textureDown->loadFromFile(texDownFilename);
        m_textureDownPtr = m_textureDown.get();
    }

    if (texHoverFilename != std::string("UNDEFINED")) {
        m_textureHover = std::shared_ptr<sf::Texture>(new sf::Texture());
        m_textureHover->loadFromFile(texHoverFilename);
        m_textureHoverPtr = m_textureHover.get();
    }

    HImageButtonPostInit();
}

HImageButton::HImageButton(std::shared_ptr<sf::Texture> sharedUp, std::shared_ptr<sf::Texture> sharedDown, std::shared_ptr<sf::Texture> sharedHover)
    : HImage(sharedUp)
    , m_textureDown(sharedDown)
    , m_textureDownPtr(sharedDown.get())
    , m_textureHover(sharedHover)
    , m_textureHoverPtr(sharedHover.get())
    , m_spriteDown(nullptr)
    , m_spriteHover(nullptr)
    , m_buttonState(EButtonState::UP) {

    HImageButtonPostInit();
}

void HImageButton::AddText(const std::string& text, const sf::Font* font, const sf::Color& fontColor, unsigned int characterSize) {
    if (m_text == nullptr) {
        m_text = std::unique_ptr<HText>(new HText(text, font, characterSize, fontColor));
    } else {
        // Needs extended functions in HText to implement, TODO
    }

    m_text->OnCalculateSize();
}

void HImageButton::FitToText(bool symmetrical, float hPadding, float vPadding) {
    m_fitToText = true;
    m_fitToTextSymmetrical = symmetrical;
    m_fitToTextHPadding = hPadding;
    m_fitToTextVPadding = vPadding;

    RequestSizeRecalculation();
    RequestLayoutInvalidation();
}

void HImageButton::FitToTexture() {
    m_fitToText = false;

    RequestSizeRecalculation();
    RequestLayoutInvalidation();
}

void HImageButton::HImageButtonPostInit() {
    // Scale sprites to 'up' sprite size to get uniform size
    if (m_textureDownPtr != nullptr) {
        m_spriteDown = std::unique_ptr<sf::Sprite>(new sf::Sprite(*m_textureDownPtr));
        m_spriteDown->setScale(static_cast<float>(GetTexture()->getSize().x) / static_cast<float>(m_textureDownPtr->getSize().x),
                               static_cast<float>(GetTexture()->getSize().y) / static_cast<float>(m_textureDownPtr->getSize().y));
    }

    if (m_textureHoverPtr != nullptr) {
        m_spriteHover = std::unique_ptr<sf::Sprite>(new sf::Sprite(*m_textureHoverPtr));
        m_spriteHover->setScale(static_cast<float>(GetTexture()->getSize().x) / static_cast<float>(m_textureHoverPtr->getSize().x),
                                static_cast<float>(GetTexture()->getSize().y) / static_cast<float>(m_textureHoverPtr->getSize().y));
    }

    m_fitToText = false;
    m_fitToTextSymmetrical = false;
    m_fitToTextHPadding = 0.f;
    m_fitToTextVPadding = 0.f;

    AddMouseInputReceiver();

    // Default mouse behavior
    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseEnter, [this] (HComponent* self) {
        m_buttonState = EButtonState::HOVER;
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeave, [this] (HComponent* self) {
        m_buttonState = EButtonState::UP;
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftDown, [this] (HComponent* self) {
        m_buttonState = EButtonState::DOWN;
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftUp, [this] (HComponent* self) {
        m_buttonState = EButtonState::HOVER;
    });

    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftReset, [this] (HComponent* self) {
        m_buttonState = EButtonState::UP;
    });
}

void HImageButton::OnCalculateSize() {
    if (!m_fitToText || m_text == nullptr) {
        sf::Vector2f textureSize = sf::Vector2f(
                                       static_cast<float>(GetTexture()->getSize().x),
                                       static_cast<float>(GetTexture()->getSize().y)
                                   );

        SetSize(textureSize);

        sf::Vector2f scaleFactor = sf::Vector2f(
                                       textureSize.x / GetSprite()->getLocalBounds().width,
                                       textureSize.y / GetSprite()->getLocalBounds().height
                                   );

        GetSprite()->setScale(sf::Vector2f(
                                  textureSize.x / GetSprite()->getLocalBounds().width,
                                  textureSize.y / GetSprite()->getLocalBounds().height
                              ));

        if (m_spriteDown != nullptr) {
            sf::Vector2f textureSizeDown = sf::Vector2f(static_cast<float>(m_textureDownPtr->getSize().x), static_cast<float>(m_textureDownPtr->getSize().y));
            m_spriteDown->setScale(sf::Vector2f(
                                       textureSize.x / textureSizeDown.x,
                                       textureSize.y / textureSizeDown.y
                                   ));
        }

        if (m_spriteHover != nullptr) {
            sf::Vector2f textureSizeHover = sf::Vector2f(static_cast<float>(m_textureHoverPtr->getSize().x), static_cast<float>(m_textureHoverPtr->getSize().y));
            m_spriteHover->setScale(sf::Vector2f(
                                        textureSize.x / textureSizeHover.x,
                                        textureSize.y / textureSizeHover.y
                                    ));
        }
    } else {
        sf::Vector2f textSize = m_text->GetTextSize();
        sf::Vector2f texUpSize = sf::Vector2f(static_cast<float>(GetTexture()->getSize().x), static_cast<float>(GetTexture()->getSize().y));

        sf::Vector2f newTexSize(0.f, 0.f);

        // Determine if text dimensions on X and/or on Y are bigger as texture dimensions of 'up' texture (defined in HImage)
        if (textSize.x > texUpSize.x) {
            newTexSize.x = textSize.x;
        } else {
            newTexSize.x = texUpSize.x;
        }

        if (textSize.y > texUpSize.y) {
            newTexSize.y = textSize.y;
        } else {
            newTexSize.y = texUpSize.y;
        }

        // Adjust the text size if symmetrical requested
        if (m_fitToTextSymmetrical) {
            if (newTexSize.x >= newTexSize.y) {
                // Determine scale factor of x
                float xScaleFactor = newTexSize.x / texUpSize.x;
                newTexSize.y = newTexSize.y * xScaleFactor;
            } else {
                // Determine scale factor of y
                float yScaleFactor = newTexSize.y / texUpSize.y;
                newTexSize.x = newTexSize.x * yScaleFactor;
            }
        }

        // Set rectangle for container size calculations
        SetSize(newTexSize);

        sf::Vector2f upScaleFactor(newTexSize.x / texUpSize.x, newTexSize.y / texUpSize.y);

        // Scale HImage Sprite
        GetSprite()->setScale(upScaleFactor);

        // Caluclate and perform scaling on Down and Hover
        if (m_spriteDown != nullptr) {
            sf::Vector2f textureSizeDown = sf::Vector2f(static_cast<float>(m_textureDownPtr->getSize().x), static_cast<float>(m_textureDownPtr->getSize().y));

            sf::Vector2f downScaleFactor(newTexSize.x / textureSizeDown.x, newTexSize.y / textureSizeDown.y);

            m_spriteDown->setScale(downScaleFactor);
        }

        if (m_spriteHover != nullptr) {
            sf::Vector2f textureSizeHover = sf::Vector2f(static_cast<float>(m_textureHoverPtr->getSize().x), static_cast<float>(m_textureHoverPtr->getSize().y));

            sf::Vector2f hoverScaleFactor(newTexSize.x / textureSizeHover.x, newTexSize.y / textureSizeHover.y);

            m_spriteHover->setScale(hoverScaleFactor);
        }
    }
}

void HImageButton::OnLayout() {
    HImage::OnLayout();

    if (m_spriteDown != nullptr) {
        m_spriteDown->setPosition(GetAbsolutePosition());
    }
    if (m_spriteHover != nullptr) {
        m_spriteHover->setPosition(GetAbsolutePosition());
    }
    if (m_text != nullptr) {
        sf::Vector2f textSize = m_text->GetTextSize();

        m_text->SetAbsolutePosition(sf::Vector2f(
                                        GetAbsolutePosition().x + GetCalculatedSize().x / 2.f - textSize.x / 2.f,
                                        GetAbsolutePosition().y + GetCalculatedSize().y / 2.f - textSize.y / 2.f
                                    ));
    }
}

void HImageButton::Draw(sf::RenderTarget& rw) {
    if (m_buttonState == EButtonState::UP || (m_buttonState == EButtonState::DOWN && m_spriteDown == nullptr) || (m_buttonState == EButtonState::HOVER && m_spriteHover == nullptr)) {
        HImage::Draw(rw);
    } else if (m_buttonState == EButtonState::DOWN && m_spriteDown != nullptr) {
        rw.draw(*m_spriteDown);
    } else if (m_buttonState == EButtonState::HOVER && m_spriteHover != nullptr) {
        rw.draw(*m_spriteHover);
    }

    if (m_text != nullptr) {
        m_text->Draw(rw);
    }
}

void HImageButton::RegisterCallback(std::function<void (HComponent*)> functionObject) {
    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseLeftUp, std::move(functionObject));
}

void HImageButton::RegisterAltCallback(std::function<void (HComponent*)> functionObject) {
    GetMouseInputReceiver()->RegisterButtonFunction(EMouseButtonEvent::OnMouseRightUp, std::move(functionObject));
}

bool HImageButton::IsFitToText() const {
    return m_fitToText;
}

} // namespace GUI
} // namespace HSF
