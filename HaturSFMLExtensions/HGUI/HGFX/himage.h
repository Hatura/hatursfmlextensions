#pragma once

#include "HGUI/hcomponent.h"

#include <memory>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace HSF {
namespace GUI {

class HImage : public HComponent
{
public:
    /**
     * @brief HImage
     * Create from extern sf::Texture (no ownership)
     */
    explicit HImage(const sf::Texture* texturePtr);

    /**
     * @brief HImage
     * Create from filename (ownership)
     */
    explicit HImage(const std::string& fileName);

    /**
     * @brief HImage
     * Create from shared_ptr (shared ownership)
     */
    explicit HImage(std::shared_ptr<sf::Texture> sharedTexture);

    void Draw(sf::RenderTarget& rw) override;

protected:
    sf::Sprite* GetSprite() const;
    const sf::Texture* GetTexture() const;

    void OnCalculateSize() override;
    void OnLayout() override;

private:
    void HImagePostInit();

    std::shared_ptr<sf::Texture>    m_texture;          // for ownership
    mutable const sf::Texture*      m_texturePtr;       // for no ownership
    std::unique_ptr<sf::Sprite>     m_sprite;
};

} // namespace GUI
} // namespace HSF
