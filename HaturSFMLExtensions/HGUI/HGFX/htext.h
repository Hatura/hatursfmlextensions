#pragma once

#include "HGUI/hcomponent.h"

#include <memory>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>

namespace HSF {
namespace GUI {

/**
 * @brief GetTextSize
 * Helper function to determine textSize because sf::Text::GetLocalBounds is anaccurate
 */
sf::Vector2f CalculateTextSize(const sf::Text& text);

class HText : public HComponent
{
public:
    /**
     * @brief HText
     * Craete from extern sf::Text (no ownership)
     */
    explicit HText(sf::Text* textPtr, const sf::Color& backgroundColor = sf::Color::Transparent);

    /**
     * @brief HText
     * Create from string (ownership)
     */
    HText(const std::string& textString, const sf::Font* font, unsigned int textSize = 26, const sf::Color& textColor = sf::Color::White, const sf::Color& backgroundColor = sf::Color::Transparent);

    friend class HImageButton;

    void Draw(sf::RenderTarget& rw) override;

    const sf::Vector2f GetTextSize() const;
    const sf::Color& GetBackgroundColor() const;

protected:
    sf::RectangleShape& GetBackgroundShape();

private:
    std::unique_ptr<sf::Text>   m_text;
    sf::Text*                   m_textPtr;

    // sf::Text::GetLocal/GlobalBounds obiously does not work correctly
    unsigned int hFillHelperSize = 10;
    unsigned int vFillHelperSize = 4;

    sf::RectangleShape          m_backgroundShape;
    sf::Color                   m_backgroundColor;

    // Can never be virtual, ctor call
    void HTextPostInit(const sf::Color& backgroundColor);

    // Inherited from HComponent
    void OnCalculateSize() override;
    void OnLayout() override;
};

} // namespace GUI
} // namespace HSF
