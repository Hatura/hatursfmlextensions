#pragma once

#include <memory>
#include <deque>
#include <set>
#include <SFML/Graphics/Font.hpp>
#include "HUtility/hsimplegraphicholder.h"
#include "HContainer/hcontainer.h"
#include "HContainer/hwindow.h"

namespace HSF {
namespace GUI {

/**
 * @brief The EResizeBehavior enum
 *
 * TODO: Only Expand is implemented yet..
 */
enum class EResizeBehavior {
    StretchOnResize,        // SFML's default behavior for default window rendering
    ExpandOnResize,         // Expand the HGUI's view to match the new window size, desktop like
    UserDefinedReconstruct  // Do nothing, let user rebuild the UI externally .. (internally possible??)
};

class HGUI
{
public:
    HGUI(sf::RenderWindow& rw, EResizeBehavior resizeBehavior = EResizeBehavior::ExpandOnResize);

    // public method to draw/calc the entire GUI
    void Draw();

    // add / remove elements
    template<typename T>
    T* AddContainer() {
        static_assert(std::is_base_of<HContainer, T>::type, "AddContainer() T must be base of HContainer");
        static_assert(!std::is_base_of<HWindow, T>::type, "AddContainer() Use AddContainer(param) to add windows!");

        std::unique_ptr<T> container(new T());

        container->SetRoot();
        container->SetHGUI(this);

        m_containerChildren.push_back(std::move(container));

        return dynamic_cast<T*>(m_containerChildren.back().get());
    }

    template<typename T>
    T* AddContainer(std::unique_ptr<T> container) {
        container->SetRoot();
        container->SetHGUI(this);

        m_containerChildren.push_back(std::move(container));

        return dynamic_cast<T*>(m_containerChildren.back().get());
    }

    template<>
    HWindow* AddContainer<HWindow>(std::unique_ptr<HWindow> container) {
        container->SetRoot();
        container->SetHGUI(this);

        m_windowChildren.push_back(std::move(container));
        m_windowChildrenPtr.push_back(m_windowChildren.back().get());

        BringWindowToFront(m_windowChildren.back().get());

        return m_windowChildren.back().get();
    }

    void OnWindowResize();
    void RequestSafeRemoval(HContainer* container);
    void BringWindowToFront(HWindow* window);

//    void AddDefaultFont(const std::string& fontFile);
//    void AddDefaultFont(const sf::Font* font);

    const sf::Font* GetDefaultFont() const;

    void SetDebugMode(bool debug);

    const HSimpleGraphicHolder* GetSimpleGraphicsHolder() const;

    size_t GetNumWindows() const;
    size_t GetNumNonWindowContainers() const;
    size_t GetNumTotalContainers() const;

    const sf::RenderWindow& GetRenderWindow() const;
    const sf::View& GetGUIView() const;

    const sf::Vector2f& GetCachedMousePosition() const;
private:
    void RemoveFlaggedContainers();
    void ReArrangeWindows();

    std::vector<sf::Mouse::Button> GetPressedMouseButtons();

    sf::RenderWindow& m_rw;
    EResizeBehavior m_resizeBehavior;

    sf::View m_view;
    std::unique_ptr<HSimpleGraphicHolder> m_sgh;

    bool m_debugModeEnabled;

    sf::Vector2f m_lastCachedMousePosition;

    std::deque<std::unique_ptr<HContainer>> m_containerChildren;
    std::deque<std::unique_ptr<HWindow>> m_windowChildren;

    /**
     * @brief m_windowChildrenPtr
     * Used internally to order the windows
     */
    std::vector<HWindow*> m_windowChildrenPtr;

    std::set<HContainer*> m_deletionFlaggedContainers;

    bool m_rearrangeWindows;
    HWindow* m_requestedFrontWindow; 
};

} // namespace gui
} // namespace hsf
