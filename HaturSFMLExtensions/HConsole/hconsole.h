#pragma once

#include <deque>
#include <chrono>
#include <functional>
#include <map>
#include <string>
#include <SFML/Graphics.hpp>

namespace HSF {
namespace Utility {

char TranslateKey(bool shift, const sf::Keyboard::Key& key);

class HConsole final {
public:
    HConsole(const sf::RenderWindow& rw, const sf::Font& font, const sf::Color consoleBackgroundColor = sf::Color(64, 64, 64, 160));

    enum class EConsoleEntryType {
        Info,
        Warning,
        Error
    };

    void AddMessage(const std::string& message, EConsoleEntryType entryType = EConsoleEntryType::Info);
    void PrintVec(sf::Vector2f vec, const std::string& vecName = std::string("Undefined"));

    // If commands refer to [this] classmember, they need to be unregistered or they produce undefined behaviour!
    // Aliases need to be unregistered the same way commands do!
    void RegisterCommand(const std::string& callName, std::function<void(const std::vector<std::string>&)> function);
    bool RegisterAlias(const std::string& aliasName, const std::string& linkedCallname);

    bool UnregisterCommand(const std::string& callName);

    void GetInput(const sf::Keyboard::Key& key);
    void Draw(sf::RenderWindow& rw);

    void Open();
    void Close();
    void Toggle();

    bool GetIsOpen() const;

private:
    void ExecuteCommand();

    bool m_needsRearrangement;

    const sf::Font m_font;

    int m_characterSize;
    const int m_messageLimit;
    const float m_consoleHeight;
    const float m_consoleCommandEntryHeight;
    const float m_consoleFrameThickness;

    bool m_isOpen;

    int m_entryStartIndex;
    const int m_numMessagesDrawn;

    sf::Texture m_consoleBackgroundTexture;
    sf::RectangleShape m_consoleBackground;
    sf::RectangleShape m_consoleCommandBackground;

    sf::Text m_cursor;
    std::chrono::steady_clock::duration m_cursorBlinkInterval;
    std::chrono::steady_clock::time_point m_cursorLastBlinkedTime;
    bool m_cursorVisible;

    std::deque<sf::Text> m_messageEntries;

    const int m_maxCommandArguments;

    std::map<std::string, std::function<void(const std::vector<std::string>&)>> m_registeredCommands;

    sf::Text m_inputText;
    std::deque<std::string> m_previousCommands;
    int m_commandIndex;   
};

} // namespace Console
} // namespace HSF
