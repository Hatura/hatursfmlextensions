#include "hconsole.h"

namespace HSF {
namespace Utility {

char TranslateKey(bool shift, const sf::Keyboard::Key& key) {
    switch (key) {
    case sf::Keyboard::Key::A:
        if (shift) return 'A';
        else return 'a';
    case sf::Keyboard::Key::B:
        if (shift) return 'B';
        else return 'b';
    case sf::Keyboard::Key::C:
        if (shift) return 'C';
        else return 'c';
    case sf::Keyboard::Key::D:
        if (shift) return 'D';
        else return 'd';
    case sf::Keyboard::Key::E:
        if (shift) return 'E';
        else return 'e';
    case sf::Keyboard::Key::F:
        if (shift) return 'F';
        else return 'f';
    case sf::Keyboard::Key::G:
        if (shift) return 'G';
        else return 'g';
    case sf::Keyboard::Key::H:
        if (shift) return 'H';
        else return 'h';
    case sf::Keyboard::Key::I:
        if (shift) return 'I';
        else return 'i';
    case sf::Keyboard::Key::J:
        if (shift) return 'J';
        else return 'j';
    case sf::Keyboard::Key::K:
        if (shift) return 'K';
        else return 'k';
    case sf::Keyboard::Key::L:
        if (shift) return 'L';
        else return 'l';
    case sf::Keyboard::Key::M:
        if (shift) return 'M';
        else return 'm';
    case sf::Keyboard::Key::N:
        if (shift) return 'N';
        else return 'n';
    case sf::Keyboard::Key::O:
        if (shift) return 'O';
        else return 'o';
    case sf::Keyboard::Key::P:
        if (shift) return 'P';
        else return 'p';
    case sf::Keyboard::Key::Q:
        if (shift) return 'Q';
        else return 'q';
    case sf::Keyboard::Key::R:
        if (shift) return 'R';
        else return 'r';
    case sf::Keyboard::Key::S:
        if (shift) return 'S';
        else return 's';
    case sf::Keyboard::Key::T:
        if (shift) return 'T';
        else return 't';
    case sf::Keyboard::Key::U:
        if (shift) return 'U';
        else return 'u';
    case sf::Keyboard::Key::V:
        if (shift) return 'V';
        else return 'v';
    case sf::Keyboard::Key::W:
        if (shift) return 'W';
        else return 'w';
    case sf::Keyboard::Key::X:
        if (shift) return 'X';
        else return 'x';
    case sf::Keyboard::Key::Y:
        if (shift) return 'Y';
        else return 'y';
    case sf::Keyboard::Key::Z:
        if (shift) return 'Z';
        else return 'z';
    case sf::Keyboard::Key::Num0:
        return '0';
    case sf::Keyboard::Key::Num1:
        return '1';
    case sf::Keyboard::Key::Num2:
        return '2';
    case sf::Keyboard::Key::Num3:
        if (shift) return '#';
        else return '3';
    case sf::Keyboard::Key::Num4:
        return '4';
    case sf::Keyboard::Key::Num5:
        return '5';
    case sf::Keyboard::Key::Num6:
        return '6';
    case sf::Keyboard::Key::Num7:
        return '7';
    case sf::Keyboard::Key::Num8:
        return '8';
    case sf::Keyboard::Key::Num9:
        return '9';
    case sf::Keyboard::Key::Space:
        return ' ';
    case sf::Keyboard::Key::Comma:
        return ',';
    case sf::Keyboard::Key::Period:
        return '.';
    case sf::Keyboard::Key::Subtract:
    case sf::Keyboard::Key::Dash: // DE only
        return '-';
    default:
        return '\0';
    }
}

HConsole::HConsole(const sf::RenderWindow& rw, const sf::Font& font, const sf::Color consoleBackgroundColor)
    : m_font(font)
    , m_needsRearrangement(false)
    , m_consoleHeight(200.f)
    , m_consoleCommandEntryHeight(20.f)
    , m_consoleFrameThickness(1.f)
    , m_messageLimit(100)
    , m_characterSize(12)
    , m_isOpen(false)
    , m_entryStartIndex(0)
    , m_numMessagesDrawn(12)
    , m_cursorBlinkInterval(std::chrono::milliseconds(500))
    , m_cursorLastBlinkedTime(std::chrono::steady_clock::now())
    , m_cursorVisible(true)
    , m_maxCommandArguments(5)
    , m_commandIndex(0) {

    m_consoleBackground.setSize(sf::Vector2f(rw.getSize().x - m_consoleFrameThickness * 2, m_consoleHeight));
    m_consoleBackground.setPosition(sf::Vector2f(m_consoleBackground.getPosition().x + m_consoleFrameThickness, m_consoleBackground.getPosition().y + m_consoleFrameThickness));
    m_consoleBackground.setOutlineThickness(m_consoleFrameThickness);
    m_consoleBackground.setOutlineColor(sf::Color::White);
    m_consoleBackground.setFillColor(consoleBackgroundColor);

    m_consoleCommandBackground.setSize(sf::Vector2f(rw.getSize().x - m_consoleFrameThickness * 2, m_consoleCommandEntryHeight));
    m_consoleCommandBackground.setPosition(sf::Vector2f(m_consoleCommandBackground.getPosition().x + m_consoleFrameThickness, m_consoleCommandBackground.getPosition().y + m_consoleFrameThickness + m_consoleHeight));
    m_consoleCommandBackground.setOutlineThickness(m_consoleFrameThickness);
    m_consoleCommandBackground.setOutlineColor(sf::Color::White);
    m_consoleCommandBackground.setFillColor(consoleBackgroundColor);

    m_cursor.setCharacterSize(m_characterSize);
    m_cursor.setColor(sf::Color::White);
    m_cursor.setFont(m_font);
    m_cursor.setString("_");
    m_cursor.setPosition(m_consoleCommandBackground.getPosition().x+2, m_consoleCommandBackground.getPosition().y+3);

    m_cursorLastBlinkedTime = std::chrono::steady_clock::now();

    m_inputText.setCharacterSize(m_characterSize);
    m_inputText.setColor(sf::Color::White);
    m_inputText.setFont(m_font);
    m_inputText.setString("");
    m_inputText.setPosition(m_consoleCommandBackground.getPosition().x+2, m_consoleCommandBackground.getPosition().y+3);

    RegisterCommand("exit", [] (const std::vector<std::string>& args) {
        exit(1);
    });
    RegisterAlias("quit", "exit");

    RegisterCommand("cmdlist", [this] (const std::vector<std::string>& args) {
        AddMessage(std::string("List of registered console commands, ") + std::to_string(m_registeredCommands.size()) + std::string(" commands total."));

        for (const auto& command : m_registeredCommands) {
            AddMessage(command.first);
        }
    });
    RegisterAlias("help", "cmdlist");
}

void HConsole::AddMessage(const std::string& message, EConsoleEntryType entryType) {
    sf::Text text;
    text.setString(message);
    text.setCharacterSize(m_characterSize);
    text.setFont(m_font);

    switch (entryType) {
    case EConsoleEntryType::Info:
        text.setColor(sf::Color::White);
        break;
    case EConsoleEntryType::Warning:
        text.setColor(sf::Color::Yellow);
        break;
    case EConsoleEntryType::Error:
        text.setColor(sf::Color::Red);
        break;
    default:
        text.setColor(sf::Color::White);
        break;
    }

    m_messageEntries.push_back(text);

    if (m_messageEntries.size() > m_messageLimit) {
        m_messageEntries.pop_front();
    }

    m_needsRearrangement = true;
}

void HConsole::PrintVec(sf::Vector2f vec, const std::string& vecName) {
    std::string passString = std::string("Location for " + vecName + " X: ") + std::to_string(vec.x) + std::string(", Y: ") + std::to_string(vec.y);
    AddMessage(passString);
}

void HConsole::RegisterCommand(const std::string& callName, std::function<void(const std::vector<std::string>&)> function) {
    m_registeredCommands.insert(std::make_pair(callName, function));
}

bool HConsole::RegisterAlias(const std::string& aliasName, const std::string& linkedCallname) {
    auto it = m_registeredCommands.find(linkedCallname);

    if (it != m_registeredCommands.end()) {
        m_registeredCommands.insert(std::make_pair(aliasName, m_registeredCommands.find(linkedCallname)->second));
        return true;
    } else {
        AddMessage(std::string("Couldn't register alias for command " + linkedCallname + ", command not found!"), EConsoleEntryType::Error);
        return false;
    }
}

bool HConsole::UnregisterCommand(const std::string& callName) {
    auto it = m_registeredCommands.find(callName);

    if (it != m_registeredCommands.end()) {
        m_registeredCommands.erase(callName);
        return true;
    } else {
        AddMessage(std::string("Couldn't unregister command " + callName + ", command not found!"), EConsoleEntryType::Error);
        return false;
    }
}

void HConsole::GetInput(const sf::Keyboard::Key& key) {
    bool shiftHeld = sf::Keyboard::isKeyPressed(sf::Keyboard::Key::RShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift);

    if (key == sf::Keyboard::Key::Return) {
        ExecuteCommand();
    } else if (key == sf::Keyboard::Key::BackSpace) {
        std::string tempString = m_inputText.getString();
        if (tempString.size() > 0 && sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LControl)) {
            m_inputText.setString("");
        } else if (tempString.size() > 0) {
            tempString = tempString.substr(0, tempString.size()-1);
            m_inputText.setString(tempString);
        }
    } else if (key == sf::Keyboard::Up) {
        if (m_previousCommands.size() > m_commandIndex) {
            ++m_commandIndex;
            m_inputText.setString(m_previousCommands.at(m_previousCommands.size()-m_commandIndex));
        }
    } else if (key == sf::Keyboard::Down && m_commandIndex > 1) {
        --m_commandIndex;
        m_inputText.setString(m_previousCommands[m_previousCommands.size()-m_commandIndex]);
    } else if (key == sf::Keyboard::PageUp) {
        if (m_messageEntries.size() > m_numMessagesDrawn) {
            m_entryStartIndex += 5;
            if (m_entryStartIndex > m_messageEntries.size() - m_numMessagesDrawn) {
                m_entryStartIndex = m_messageEntries.size() - m_numMessagesDrawn;
            }
            m_needsRearrangement = true;
        }
    } else if (key == sf::Keyboard::PageDown) {
        m_entryStartIndex -= 5;
        if (m_entryStartIndex < 0) {
            m_entryStartIndex = 0;
        }
        m_needsRearrangement = true;
    } else {
        char newChar = TranslateKey(shiftHeld, key);
        if (newChar != '\0') {
            m_inputText.setString(m_inputText.getString() + TranslateKey(shiftHeld, key));
        }
    }
}

void HConsole::ExecuteCommand() {
    m_commandIndex = 0; // reset

    std::string rawCommand = m_inputText.getString();
    std::transform(rawCommand.begin(), rawCommand.end(), rawCommand.begin(), ::tolower); // Not portable?

    std::string command;
    std::vector<std::string> args;

    size_t pos_w = rawCommand.find(" ");
    if (pos_w != std::string::npos) {
        command = rawCommand.substr(0, pos_w);
        std::string argString = rawCommand.substr(pos_w+1);
        int argCounter = 0;
        while (argString.find(" ") != std::string::npos) {
            args.push_back(argString.substr(0, argString.find(" ")));

            ++argCounter;
            if (argCounter >= m_maxCommandArguments) {
                break;
            }

            argString = argString.substr(argString.find(" ")+1);
        }
        // add last element | TODO: Adds remaining whitespace for now, is that a problem?
        if (argCounter < m_maxCommandArguments) {
            args.push_back(argString);
        }
    } else {
        command = rawCommand;
    }

    bool processedCommand = false;

    if (m_registeredCommands.find(command) != m_registeredCommands.end()) {
        m_registeredCommands.find(command)->second(args);
        processedCommand = true;
    }

    if (!processedCommand) {
        AddMessage(std::string("No such command: " + command), EConsoleEntryType::Warning);
    }

    m_previousCommands.push_back(rawCommand);

    m_inputText.setString("");
}

void HConsole::Draw(sf::RenderWindow& rw) {
    auto timeNow = std::chrono::steady_clock::now();

    if (timeNow - m_cursorBlinkInterval > m_cursorLastBlinkedTime) {
        m_cursorLastBlinkedTime = timeNow;
        m_cursorVisible = !m_cursorVisible;
    }

    if (m_isOpen) {
        rw.setView(rw.getDefaultView());

        rw.draw(m_consoleBackground);
        rw.draw(m_consoleCommandBackground);

        rw.draw(m_inputText);

        m_cursor.setPosition(m_inputText.getGlobalBounds().width + 4, m_cursor.getPosition().y);
        if (m_cursorVisible) {
            rw.draw(m_cursor);
        }

        int start = static_cast<int>(m_messageEntries.size()) - 10;
        if (start < 0) {
            start = 0;
        }

        int countIndex = 0;

        for (auto& it = m_messageEntries.rbegin() + m_entryStartIndex; it != m_messageEntries.rend(); ++it) {
            if (m_needsRearrangement) {
                (*it).setPosition(4, m_consoleHeight - ((countIndex+1) * (m_characterSize + 4)));
            }
            rw.draw(*it);
            ++countIndex;
            if (countIndex >= m_numMessagesDrawn) {
                break;
            }
        }

        m_needsRearrangement = false;
    }
}

void HConsole::Open() {
    m_isOpen = true;
}

void HConsole::Close() {
    m_isOpen = false;
}

void HConsole::Toggle() {
    m_isOpen = m_isOpen ? false : true;
}

bool HConsole::GetIsOpen() const {
    return m_isOpen;
}

} // namespace Console
} // namespace HSF
